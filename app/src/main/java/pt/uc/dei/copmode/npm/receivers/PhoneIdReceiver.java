package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.helpers.PhoneIdHelper;

public class PhoneIdReceiver extends BroadcastReceiver {
    private static String TAG = "PhoneIdReceiver";

    private static final String PHONE_ID_KEY = "phone_id";

    public void onReceive(Context context, Intent arg1) {
        String phoneId = arg1.getStringExtra(PHONE_ID_KEY);
        MyLogHelper.logDebug(TAG, "Received PhoneIdReceiver with id: " + phoneId);
        PhoneIdHelper.storePhoneId(context, phoneId);
        Toast.makeText(context, "Received phoneId = " + phoneId, Toast.LENGTH_LONG).show();
    }
}
