package pt.uc.dei.copmode.npm.components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pt.uc.dei.copmode.npm.BuildConfig;
import pt.uc.dei.copmode.npm.annotations.ExcludeFromUpload;
import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAndContextData;
import pt.uc.dei.copmode.npm.helpers.DateHelper;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.helpers.PhoneIdHelper;


public class Uploader {
    private static final String TAG = "Uploader";

    // shared preferences constants
    private static final String UPLOADS_PREFERENCES_FILE = BuildConfig.APPLICATION_ID + ".uploads_info";

    /**
     * Key to store/retrieve id for the new upload file. This id is incremented for every upload.
     */
    private static final String UPLOADS_PREFERENCES_NEW_FILE_ID_KEY = "new_file_id_key";

    /**
     * Key to store/retrieve the timestamp of the last upload from the shared preferences.
     * This timestamp is used to show to the user the date of the last upload.
     */
    private static final String LAST_UPLOAD_TIMESTAMP_KEY = "last_upload_timestamp_key";

    /**
     * Key to store/retrieve the timestamp that is higher than all uploaded data and lower than all non uploaded data.
     * When retrieving the non-uploaded data from the DB, we always get the permission data since this timestamp
     */
    private static final String MIN_NON_UPLOADED_DATA_TIMESTAMP = "min_non_uploaded_data_timestamp";

    // worker thread
    private static final String PERIODIC_UPLOAD_WORK_NAME = BuildConfig.APPLICATION_ID + ".uploadworker";
    private static final String PERIODIC_UPLOAD_WORK_TAG = PERIODIC_UPLOAD_WORK_NAME;
    private static final long PERIODIC_UPLOAD_PERIODICITY_HOURS = 1L;

    private static final String UPLOADS_DIRECTORY = "uploads";

    private static volatile Uploader instance;

    private LiveData<List<PermissionDialogAndContextData>> dataToUploadLiveData;
    private Observer<List<PermissionDialogAndContextData>> dataToUploadLiveDataObserver;  // avoids recreating
    private LiveData<WorkInfo> periodicUploadingState;
    private LiveData<WorkInfo> singleUploadingState;

    private Uploader(Context context) {

        asyncUpdateAndObserveData(context);

        Constraints constraints = new Constraints.Builder()
                .setRequiresDeviceIdle(true)  // we upload only when the device is idle as this is a work intensive operation
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        // note that the WorkManager stops the work if the constraint is no longer met and retried later when met again. Thus, the work might fail often.
        PeriodicWorkRequest uploadWorkRequest =
                new PeriodicWorkRequest.Builder(Uploader.UploadWorker.class, PERIODIC_UPLOAD_PERIODICITY_HOURS, TimeUnit.HOURS)
                        .addTag(PERIODIC_UPLOAD_WORK_TAG)
                        .setConstraints(constraints)
                        .build();

        WorkManager workManager = WorkManager.getInstance(context);
        workManager.enqueueUniquePeriodicWork(PERIODIC_UPLOAD_WORK_NAME, ExistingPeriodicWorkPolicy.KEEP, uploadWorkRequest);
        periodicUploadingState = workManager.getWorkInfoByIdLiveData(uploadWorkRequest.getId());
    }

    /**
     * If there are observers of the previous dataToUploadLiveData, these will be lost.
     * For this reason, this function should only be called after an update
     * @param context
     */
    private void asyncUpdateAndObserveData(Context context) {
        if (dataToUploadLiveDataObserver == null) {
            dataToUploadLiveDataObserver = new Observer<List<PermissionDialogAndContextData>>() {
                @Override
                public void onChanged(List<PermissionDialogAndContextData> permissionDialogAndContextData) {
                    // we don't need to save it since we can always retrieve it from the LiveData.
                    MyLogHelper.logDebug(TAG, "Data to upload changed.");
                }
            };
        }

        // is async since it returns LiveData
        dataToUploadLiveData = NPMDatabase.getDB(context).permissionDialogDataDao().getPermissionDialogAndContextDataSinceTimestamp(getMinNonUploadedDataTimestamp(context));
        if(Looper.myLooper() == Looper.getMainLooper())
            dataToUploadLiveData.observeForever(dataToUploadLiveDataObserver);
        else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {  // we can only observe in the main thread
                @Override
                public void run() {
                    dataToUploadLiveData.observeForever(dataToUploadLiveDataObserver);
                }
            });
        }
    }

    public static Uploader getInstance(Context context) {
        if (instance == null) {
            synchronized (Uploader.class) {
                if (instance == null) {
                    instance = new Uploader(context);
                }
            }
        }
        return instance;
    }

    /**
     * Enqueues a one time work request to upload the data if there is data for upload and if is not
     * already uploading.
     *
     * @param context to retrieve WorkManager
     * @return A LiveData<WorkInfo> with the state of the enqueued work, or null if there's no data to upload
     */
    @Nullable
    public LiveData<WorkInfo> tryEnqueueSingleUploadWork(Context context) {
        LiveData<WorkInfo> runningWorkInfoLiveData = getRunningWorkInfoLiveData();
        if (runningWorkInfoLiveData != null)
            return runningWorkInfoLiveData;  // note that a periodic service never reaches SUCCEEDED state! todo: how to handle?

        if (existsDataForUpload()) {
            Constraints constraints = new Constraints.Builder()  // in this case we require only net access
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();
            WorkRequest uploadWorkRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                    .addTag(PERIODIC_UPLOAD_WORK_TAG)
                    .setConstraints(constraints)
                    .build();
            WorkManager workManager = WorkManager.getInstance(context);
            workManager.enqueue(uploadWorkRequest);
            singleUploadingState = workManager.getWorkInfoByIdLiveData(uploadWorkRequest.getId());
            return singleUploadingState;
        }
        return null;
    }

    public long getLastUploadTimestamp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(UPLOADS_PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(LAST_UPLOAD_TIMESTAMP_KEY, 0);  // 0 means no previous upload
    }

    @SuppressLint("ApplySharedPref")
    private void updateLastUploadTimestamp(Context context, long newValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(UPLOADS_PREFERENCES_FILE, Context.MODE_PRIVATE);
        long currentValue = sharedPreferences.getLong(LAST_UPLOAD_TIMESTAMP_KEY, 0);
        if (newValue < currentValue) {
            MyLogHelper.logError(TAG, "Bug: trying to set a last upload timestamp sooner than the previous value");
            return;
        }
        sharedPreferences.edit().putLong(LAST_UPLOAD_TIMESTAMP_KEY, newValue).commit();
    }

    private long getMinNonUploadedDataTimestamp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(UPLOADS_PREFERENCES_FILE, Context.MODE_PRIVATE);
        // MyLogHelper.logDebug(TAG, "returning " + sharedPreferences.getLong(MIN_NON_UPLOADED_DATA_TIMESTAMP, 0));
        return sharedPreferences.getLong(MIN_NON_UPLOADED_DATA_TIMESTAMP, 0);  // 0 means no previous upload
    }

    @SuppressLint("ApplySharedPref")
    private void updateMinNonUploadedDataTimestamp(Context context, long newValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(UPLOADS_PREFERENCES_FILE, Context.MODE_PRIVATE);
        long currentValue = sharedPreferences.getLong(MIN_NON_UPLOADED_DATA_TIMESTAMP, 0);
        if (newValue < currentValue) {
            MyLogHelper.logError(TAG, "Bug: trying to set a min non uploaded timestamp sooner than the previous value");
            return;
        }
        sharedPreferences.edit().putLong(MIN_NON_UPLOADED_DATA_TIMESTAMP, newValue).commit();
    }

    public boolean existsDataForUpload() {
        return dataToUploadLiveData != null && dataToUploadLiveData.getValue() != null && !dataToUploadLiveData.getValue().isEmpty();
    }

    public int getNumberOfPermissionDataToUpload() {
        if (!existsDataForUpload())
            return 0;
        return dataToUploadLiveData.getValue().size();
    }

    @Nullable
    private LiveData<WorkInfo> getRunningWorkInfoLiveData() {
        if (periodicUploadingState.getValue() != null && periodicUploadingState.getValue().getState() == WorkInfo.State.RUNNING)
            return periodicUploadingState;
        if (singleUploadingState != null && singleUploadingState.getValue() != null && singleUploadingState.getValue().getState() == WorkInfo.State.RUNNING)
            return singleUploadingState;
        return null;
    }

    private boolean isCurrentlyUploading() {
        return getRunningWorkInfoLiveData() != null;
    }

    public static class UploadWorker extends Worker {
        private static final String TAG = "UploadWorker";

        private OkHttpClient httpClient = new OkHttpClient().newBuilder().build();
        private static final String UPLOAD_BASE_URL = "https://cop-mode.dei.uc.pt/api/v1/upload/";

        private static final String DEVICE_ID_KEY = "device_id_key";

        private static final long WAKELOCK_TIMEOUT = 10*60*1000L;  // 10 minutes
        private static final String WAKELOCK_TAG = "CM-NPM::UploadWakelockTag";

        public static final String ERROR_OUTPUT_DATA_KEY = "error_output_data_key";

        public UploadWorker(
                @NonNull Context context,
                @NonNull WorkerParameters params) {
            super(context, params);
        }

        @NonNull
        @Override
        public Result doWork() {
            MyLogHelper.logDebug(TAG, "Starting upload process.");
            Context applicationContext = getApplicationContext();
            PowerManager powerManager = (PowerManager) applicationContext.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_TAG);
            wakeLock.acquire(WAKELOCK_TIMEOUT);

            Uploader uploaderInstance = getInstance(applicationContext);

            if (uploaderInstance.isCurrentlyUploading()) {
                // A periodic or user initiated upload might already be running
                MyLogHelper.logDebug(TAG, "UploadWorker started while an upload is already on-going. Exiting");
                wakeLock.release();
                return Result.success();
            }

            if(uploaderInstance.existsDataForUpload()) {
                MyLogHelper.logDebug(TAG, "Data for upload exists. Starting upload.");
                try {
                    long currentTimestamp = System.currentTimeMillis();
                    List<PermissionDialogAndContextData> data = uploaderInstance.dataToUploadLiveData.getValue();

                    // because it is ordered descendant we can get the first timestamp. We add 1 to make sure we have no duplicates between uploads
                    long newMinNonUploadedDataTimestamp = Objects.requireNonNull(data).get(0).permissionDialogData.timestamp + 1;

                    File toUploadFile = writeDataToGzipJson(data, uploaderInstance, currentTimestamp, applicationContext);
                    try (Response httpResponse = uploadFile(toUploadFile, applicationContext)) {
                        if (!httpResponse.isSuccessful()) {
                            String errorMessage = "Failed to send data to server. HTTP Error (" + httpResponse.code() + "): " + httpResponse.body().string();
                            MyLogHelper.logError(TAG, errorMessage);
                            Data errorOutput = new Data.Builder().putString(ERROR_OUTPUT_DATA_KEY, errorMessage).build();
                            wakeLock.release();
                            return Result.failure(errorOutput);  // network/server issue. We could retry here, but it is better for to user to manually try again or wait for next period.
                        }
                    }
                    MyLogHelper.logDebug(TAG, "Upload successful");

                    uploaderInstance.updateLastUploadTimestamp(applicationContext, currentTimestamp);
                    uploaderInstance.updateMinNonUploadedDataTimestamp(applicationContext, newMinNonUploadedDataTimestamp);
                    uploaderInstance.asyncUpdateAndObserveData(applicationContext);
                } catch (Exception ex) {
                    String errorMessage = "Failed to upload data: " + ex;
                    MyLogHelper.logError(TAG, errorMessage);
                    Data errorOutput = new Data.Builder().putString(ERROR_OUTPUT_DATA_KEY, errorMessage).build();
                    wakeLock.release();
                    return Result.failure(errorOutput);
                }
            }
            MyLogHelper.logDebug(TAG, "Upload process complete.");
            wakeLock.release();
            return Result.success();
        }


        private File writeDataToGzipJson(List<PermissionDialogAndContextData> data, Uploader uploaderInstance, long currentTimestamp, Context context) throws Exception {
            File baseUploadDir = new File(context.getFilesDir(), UPLOADS_DIRECTORY);
            createCleanDirectory(baseUploadDir);

            long newFileId = getNewFileId(context);
            String uploadFileName = DateHelper.getFormattedDate(currentTimestamp, "yyyy-MM-dd") + "." + newFileId + ".json.gz";

            String uploadDataJsonString = getDataAsJson(data);
            MyLogHelper.logDebug(TAG, "Serialized upload data: " + uploadDataJsonString);

            File uploadFile = new File(baseUploadDir, uploadFileName);
            try {
                // ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                writeStringToGzipFile(uploadFile, uploadDataJsonString);
            } catch (IOException ex) {
                MyLogHelper.logWarn(TAG, "Failed to write upload data to internal storage. Attempting to write in external storage. " + ex);
                // It may fail due to insufficient space in the internal storage.
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    // external storage is available for write
                    File alternativeUploadDir = new File(context.getExternalFilesDir(null), UPLOADS_DIRECTORY);
                    uploadFile = new File(alternativeUploadDir, uploadFileName);
                    writeStringToGzipFile(uploadFile, uploadDataJsonString);
                } else {
                    MyLogHelper.logError(TAG, "Failed to write upload data to file");
                    throw new IOException("Failed to write compressed gzip file due to lack of space.");
                }
            }
            MyLogHelper.logDebug(TAG, "Upload file written to " + uploadFile.toString());
            return uploadFile;
        }

        /**
         * Because we don't want to lose data due to conflicts in the server due to file naming,
         * each file is created with a unique id, which we store in and retrieved from the shared preferences.
         *
         * @param context the context to access shared preferences
         * @return a unique file id
         */
        @SuppressLint("ApplySharedPref")
        private long getNewFileId(Context context) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(UPLOADS_PREFERENCES_FILE, Context.MODE_PRIVATE);
            long newId = sharedPreferences.getLong(UPLOADS_PREFERENCES_NEW_FILE_ID_KEY, 0L);
            sharedPreferences.edit().putLong(UPLOADS_PREFERENCES_NEW_FILE_ID_KEY, newId + 1).commit();
            return newId;
        }

        private void writeStringToGzipFile(File file, String data) throws IOException {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(fileOutputStream);
            gzipOutputStream.write(data.getBytes());
            gzipOutputStream.close();
            fileOutputStream.close();
        }

        private void createCleanDirectory(File directory) {
            if (!directory.exists()) {
                directory.mkdir();
            } else {
                // clean the upload directory
                for(File file: directory.listFiles())
                    if (!file.isDirectory())
                        file.delete();
            }
        }

        private String getDataAsJson(List<PermissionDialogAndContextData> data) {
            Gson gson = new GsonBuilder().setExclusionStrategies(exclusionStrategy).create();
            return gson.toJson(data);
        }

        private Response uploadFile(File toUploadFile, Context context) throws IOException, InterruptedException {
            String phoneId = PhoneIdHelper.getPhoneId(context);
            String url = UPLOAD_BASE_URL + phoneId;

            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("data", toUploadFile.getName(),
                            RequestBody.create(toUploadFile, MediaType.get("application/gzip; charset=utf-8")))
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            // synchronous call
            return httpClient.newCall(request).execute();
        }

        /**
         * The id of the device is the truncated 8 most significant bytes of the SHA256 of the serial number.
         * To get the serial number, we need to call the console with super user.
         * To avoid repeating this process, we store the device id in the shared preferences.
         *
         * @param context the context to retrieve the shared preferences
         * @return the device id
         */
        private String generateAndStoreDeviceId(Context context) throws IOException, InterruptedException {
            String deviceId = "";

            Process su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());
            InputStream responseInputStream = su.getInputStream();

            outputStream.writeBytes("getprop ro.serialno\n");
            outputStream.flush();

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length = 0;
            while ((length = responseInputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, length);
            }
            deviceId = baos.toString("UTF-8");
            MyLogHelper.logDebug(TAG, "Got device Id = " + deviceId);

            outputStream.close();
            responseInputStream.close();

            SharedPreferences sharedPreferences = context.getSharedPreferences(UPLOADS_PREFERENCES_FILE, Context.MODE_PRIVATE);
            sharedPreferences.edit().putString(DEVICE_ID_KEY, deviceId).apply();

            return deviceId;
        }
    }

    private static final ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(ExcludeFromUpload.class) != null;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };
}
