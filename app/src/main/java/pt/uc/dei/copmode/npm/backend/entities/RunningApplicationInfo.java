package pt.uc.dei.copmode.npm.backend.entities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.pm.PackageInfoCompat;
import androidx.room.Ignore;

import java.util.Objects;

import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class RunningApplicationInfo {
    @Ignore
    private static final String TAG = "RunningApplicationInfo";

    // we don't use primitives to avoid json writes when value is null
    @Nullable  // the package manager may fail to get the name
    public String name;
    @Ignore
    private static final String nameKey = "running_application_info_name";
    @NonNull
    public String packageName;
    @Ignore
    private static final String packageNameKey = "running_application_info_package_name";
    @Nullable
    public Long longVersionCode;
    @Ignore
    private static final String longVersionCodeKey = "running_application_info_version_code";
    @Nullable
    public Integer flags;
    @Ignore
    private static final String flagsKey = "running_application_info_flags";
    @Nullable
    public Boolean isSystemApp;
    @Ignore
    private static final String isSystemAppKey = "running_application_info_is_system_app";
    @Nullable
    public Boolean isForeground;
    @Ignore
    private static final String isForegroundKey = "running_application_info_is_foreground";
    @NonNull
    public Integer category = -1;  // equals CATEGORY_UNDEFINED
    @Ignore
    private static final String categoryKey = "running_application_info_category";
    @Nullable
    public Integer uid;
    @Ignore
    private static final String uidKey = "running_application_info_uid";
    @Nullable
    public Integer pid;
    @Ignore
    private static final String pidKey = "running_application_info_pid";

    // for room
    public RunningApplicationInfo() {}

    @Ignore
    public RunningApplicationInfo(@NonNull String packageName, @Nullable Boolean isForeground, @Nullable Integer pid, @NonNull Context context) {
        this.packageName = packageName;
        this.pid = pid;
        this.isForeground = isForeground;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
            longVersionCode = PackageInfoCompat.getLongVersionCode(packageInfo);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if (applicationInfo == null) {
                MyLogHelper.logError(TAG, "null applicationInfo when retrieving RunningApplicationInfo");
                return;
            }
            name = (String) applicationInfo.loadLabel(pm);
            flags = applicationInfo.flags;
            isSystemApp = ((flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                category = applicationInfo.category;
            }
            uid = applicationInfo.uid;
        } catch (PackageManager.NameNotFoundException e) {
            MyLogHelper.logWarn(TAG, "Failed to retrieve application info from package name: " + e);
        }
    }

    @Ignore
    public RunningApplicationInfo(RunningApplicationInfo original) {
        this(original.name, original.packageName, original.longVersionCode, original.flags,
                original.isSystemApp, original.isForeground, original.category, original.uid, original.pid);
    }

    private RunningApplicationInfo(@Nullable String name, @NonNull String packageName, @Nullable Long longVersionCode, @Nullable Integer flags,
                                   @Nullable Boolean isSystemApp, @Nullable Boolean isForeground, @NonNull Integer category,
                                   @Nullable Integer uid, @Nullable Integer pid) {
        this.name = name;
        this.packageName = packageName;
        this.longVersionCode = longVersionCode;
        this.flags = flags;
        this.isSystemApp = isSystemApp;
        this.isForeground = isForeground;
        this.category = category;
        this.uid = uid;
        this.pid = pid;
    }

    @Nullable
    static RunningApplicationInfo fromBundle(@Nullable Bundle bundle) {
        if (bundle == null)
            return null;

        String name = bundle.getString(nameKey);
        String packageName = Objects.requireNonNull(bundle.getString(packageNameKey));
        Long longVersionCode = null;
        if (bundle.containsKey(longVersionCodeKey))
            longVersionCode = bundle.getLong(longVersionCodeKey);
        Integer flags = null;
        if (bundle.containsKey(flagsKey))
            flags = bundle.getInt(flagsKey);
        Boolean isSystemApp = null;
        if (bundle.containsKey(isSystemAppKey))
            isSystemApp = bundle.getBoolean(isSystemAppKey);
        Boolean isForeground = null;
        if (bundle.containsKey(isForegroundKey))
            isForeground = bundle.getBoolean(isForegroundKey);
        Integer category = bundle.getInt(categoryKey);
        Integer uid = null;
        if (bundle.containsKey(uidKey))
            uid = bundle.getInt(uidKey);
        Integer pid = null;
        if (bundle.containsKey(pidKey))
            pid = bundle.getInt(pidKey);
        return new RunningApplicationInfo(name, packageName, longVersionCode, flags, isSystemApp, isForeground, category, uid, pid);
    }

    @NonNull
    Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(nameKey, name);
        bundle.putString(packageNameKey, packageName);
        if (longVersionCode != null)
            bundle.putLong(longVersionCodeKey, longVersionCode);
        if (flags != null)
            bundle.putInt(flagsKey, flags);
        if (isSystemApp != null)
            bundle.putBoolean(isSystemAppKey, isSystemApp);
        if (isForeground != null)
            bundle.putBoolean(isForegroundKey, isForeground);
        bundle.putInt(categoryKey, category);
        if (uid != null)
            bundle.putInt(uidKey, uid);
        if (pid != null)
            bundle.putInt(pidKey, pid);
        return bundle;
    }
}
