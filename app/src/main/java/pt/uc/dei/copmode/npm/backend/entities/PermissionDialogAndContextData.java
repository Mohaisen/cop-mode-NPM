package pt.uc.dei.copmode.npm.backend.entities;

import androidx.room.Embedded;
import androidx.room.Relation;

public class PermissionDialogAndContextData {
    @Embedded public PermissionDialogData permissionDialogData;
    @Relation(
            parentColumn = "permissionDialogDataId",
            entityColumn = "permissionDialogDataOwnerId"
    )
    public ContextData contextData;
}
