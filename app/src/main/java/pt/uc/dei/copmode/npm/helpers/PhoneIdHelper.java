package pt.uc.dei.copmode.npm.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import pt.uc.dei.copmode.npm.BuildConfig;

public class PhoneIdHelper {
    private static final String TAG = "PhoneIdHelper";
    private static final String PHONE_ID_PREFERENCES_FILE = BuildConfig.APPLICATION_ID + ".phone_info";
    private static final String PHONE_ID_KEY = "phone_id";

    private static String phoneId;
    public static void storePhoneId(Context context, String phoneId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PHONE_ID_PREFERENCES_FILE, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(PHONE_ID_KEY)) {
            MyLogHelper.logError(TAG, "Attempting to reset phone_id! Bug?");
            return;
        }
        PhoneIdHelper.phoneId = phoneId;
        sharedPreferences.edit().putString(PHONE_ID_KEY, phoneId).apply();
    }

    public static String getPhoneId(Context context) {
        if (phoneId != null)
            return phoneId;

        SharedPreferences sharedPreferences = context.getSharedPreferences(PHONE_ID_PREFERENCES_FILE, Context.MODE_PRIVATE);
        phoneId = sharedPreferences.getString(PHONE_ID_KEY, null);

        if (phoneId == null) {
            if (BuildConfig.DEBUG)
                phoneId = "test_phone_id";
            else
                MyLogHelper.logError(TAG, "Retrieving null phone id, this should not happen. Was it set at install time?");
        }
        return phoneId;
    }
}
