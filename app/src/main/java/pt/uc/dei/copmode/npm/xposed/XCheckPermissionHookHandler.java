package pt.uc.dei.copmode.npm.xposed;

import android.app.AndroidAppHelper;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.Process;
import android.os.RemoteException;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import de.robv.android.xposed.XC_MethodHook;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.backend.entities.RunningApplicationInfo;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

public class XCheckPermissionHookHandler extends XC_MethodHook {
    private final static String TAG = "xposed/XCheckPermissionHookHandler";

    private static Context applicationContext;
    private static String packageName;

    private static PowerManager powerManager;
    private static KeyguardManager keyguardManager;

    // this should be immutable, but that would require a dependency
    private static final HashSet<String> permissionGroupSet = new HashSet<>(Arrays.asList(
            // to avoid api requirements, we set this as a string list taken from the Manifest class
            "android.permission-group.ACTIVITY_RECOGNITION",
            "android.permission-group.CALENDAR",
            "android.permission-group.CALL_LOG",
            "android.permission-group.CAMERA",
            "android.permission-group.CONTACTS",
            "android.permission-group.LOCATION",
            "android.permission-group.MICROPHONE",
            "android.permission-group.PHONE",
            "android.permission-group.SENSORS",
            "android.permission-group.SMS",
            "android.permission-group.STORAGE"
    ));

    private Messenger npmServiceInMessenger; // receives message from service

    private static final long bindingTimeoutSeconds = 5; // an ANR occurs after 5 seconds of non response to input

    // because multiple hooks can be called for single permission request (due to internal calls to different checkPermission methods), we must process it as a single request
    // internal calls will have the same thread ID. Note that a single app can have multiple threads requesting the same permission, but
    // a single thread can have only 1 request as it is synchronous (other calls will be internal calls).
    private static final Map<Integer, Pair<String, String>> threadIdToMethodAndCheckingPermissionPair = new HashMap<>();

    /**
     * This is a cache that is stored in memory. This cache is used to avoid re-asking NPM for a permission that
     * was previously asked. It essentially has the lifecycle of the application.
     */
    private static final HashMap<String, XCachedPermissionResult> permissionGroupCheckCacheMap = new HashMap<>();

    /**
     * This map contains the data that is received from the NPM service, such that the locked threads (waiting for dialog results) can consume it.
     */
    private static final Map<Integer, PermissionDialogData> threadIdToPermissionDialogDataMap = new HashMap<>();

    /**
     * This map contains the permissionGroup being checked (key) and the threadId that is handling the check (value).
     * It is to be used to avoid multiple concurrent permission dialogs for the same permission group.
     */
    private static final Map<String, Integer> permissionGroupToHandlerThreadIdMap = new HashMap<>();

    /**
     * Due to possible concurrent requests, we lock threads asking for permissions within the same permission group.
     * This map contains for each permissionGroup (key), the locked latch of threads that are awaiting for dialog results.
     */
    private static final Map<String, CountDownLatch> permissionGroupToResolutionLatch = new HashMap<>();

    @Override
    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
        if (applicationContext == null) {
            applicationContext = AndroidAppHelper.currentApplication().getApplicationContext();
            packageName = applicationContext.getPackageName();
            // We start the receiving Messenger in a separate thread to avoid a deadlock when waiting for the dialog resolution
            // async thread to receive the prompt results from the service
            HandlerThread handlerThread = new HandlerThread("CheckPermissionMessengerHandlerThread");
            // XposedLogger.logDebug(TAG, packageName, "Handler thread named " + handlerThread.getName() + " just started");
            handlerThread.start();
            IncomingHandler incomingHandler = new IncomingHandler(handlerThread.getLooper());
            npmServiceInMessenger = new Messenger(incomingHandler);
        }

        String checkedPermission = getCheckedPermissionFromArgs(param.args);
        if (checkedPermission == null) {
            // this will never happen. Just for safety
            XposedLogger.logError(TAG, packageName, "Failed to retrieve the requested permission from arguments");
            return;
        }

        if (!isBaseDangerousPermission(checkedPermission)) {
            XposedLogger.logDebug(TAG, packageName, "Checked permission is not a base dangerous permission (" + checkedPermission + ")");
            return;
        }

        int threadId = Process.myTid();
        synchronized (threadIdToMethodAndCheckingPermissionPair) {
            Pair<String, String> threadMethodAndCheckingPermissionPair = threadIdToMethodAndCheckingPermissionPair.get(threadId);
            if (threadMethodAndCheckingPermissionPair == null) {
                threadMethodAndCheckingPermissionPair = new Pair<>(param.method.toString(), checkedPermission);
                threadIdToMethodAndCheckingPermissionPair.put(threadId, threadMethodAndCheckingPermissionPair);
            } else {
                if (threadMethodAndCheckingPermissionPair.second.equals(checkedPermission)) {
                    XposedLogger.logDebug(TAG, packageName, "Permission already being processed in upstream method (" + param.method.toString() + "). Returning.");
                } else {
                    // this only happens if there's a bug
                    XposedLogger.logError(TAG, packageName, "Thread is checking a different permission than expected. Inconsistent state... Returning");
                }
                return;  // thread already processing this permission
            }
        }
        if (param.method.toString().contains("Calling")) {
            /*
            checkCallingPermission or checkCallingOrSelfPermission are functions used for IPC, where services verify whether the caller has permissions.
            In short, these functions protect privilege escalation. However, the services still need to have the permission to access and will call checkPermission later.
            Thus, we don't handle these "Calling" functions. Note however that we still add this to threadIdToMethodAndCheckingPermissionPair
            because we want to ignore not only this functions call but also downstream calls (to checkPermission) originating from this.
             */
            XposedLogger.logInfo(TAG, packageName, "Method " + param.method.getName() + " ignored.");
            return;
        }

        // if it reaches here, we must handle
        String checkedPermissionGroup = XposedHelper.getPermissionGroup(applicationContext, checkedPermission);
        // check if other thread is concurrently processing this permission group
        if(isBeingHandled(checkedPermissionGroup, threadId)) {
            try {
                XposedLogger.logDebug(TAG, packageName, "Other thread is currently handling this permission group (" + checkedPermissionGroup + "). Waiting for its resolution.");
                getPermissionGroupResolutionLatch(checkedPermissionGroup).await();
                // after unlocking, the result should be in the cache
            } catch (InterruptedException ignored) { }  // interruption or not, we follow the normal course of action. If has waited, the result will be cached if not, it will prompt the user as normally.
        }

        String error = handle(checkedPermission, checkedPermissionGroup, param);
        if (error != null) {
            XposedLogger.logError(TAG, packageName, "Failed to handle " + checkedPermission + ". Error: " + error);
        }
        unlockThreadsWaitingForPermissionGroupResolution(checkedPermissionGroup);
    }

    private boolean shouldPrompt() {
        if(powerManager == null)
            powerManager = (PowerManager) applicationContext.getSystemService(Context.POWER_SERVICE);
        if(powerManager != null && !powerManager.isInteractive()) {
            return false;
        } else if(powerManager != null && powerManager.isInteractive()) {
            // screen is interactive but might be locked
            if (keyguardManager == null)
                keyguardManager = ((KeyguardManager) applicationContext.getSystemService(Context.KEYGUARD_SERVICE));
            return keyguardManager == null || !keyguardManager.isKeyguardLocked();
        }
        return true;
    }

    @Override
    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
        int threadId = Process.myTid();
        synchronized (threadIdToMethodAndCheckingPermissionPair) {
            Pair<String, String> threadMethodAndCheckingPermissionPair = threadIdToMethodAndCheckingPermissionPair.get(threadId);
            if (threadMethodAndCheckingPermissionPair == null) {
                return;  // normal permission
            } else {
                if (threadMethodAndCheckingPermissionPair.first.equals(param.method.toString())) {
                    // thread finished checking the permission
                    threadIdToMethodAndCheckingPermissionPair.remove(threadId);
                    XposedLogger.logDebug(TAG, packageName, "Thread finished checking permission " + threadMethodAndCheckingPermissionPair.second);
                }
            }
        }
    }

    private boolean isBaseDangerousPermission(String checkedPermission) {
        PackageManager pm = applicationContext.getPackageManager();
        try {
            PermissionInfo permissionInfo = pm.getPermissionInfo(checkedPermission, 0);

            int protection;
            if (android.os.Build.VERSION.SDK_INT >= 28) {
                protection = permissionInfo.getProtection();
            } else {
                protection = permissionInfo.protectionLevel & PermissionInfo.PROTECTION_MASK_BASE;
            }
            if (protection != PermissionInfo.PROTECTION_DANGEROUS)
                return false;  // we only care about dangerous permissions

            if (!permissionGroupSet.contains(permissionInfo.group))
                return false;  // we only care about the base permission groups
        } catch (PackageManager.NameNotFoundException e) {
            XposedLogger.logWarn(TAG, packageName, "Package Manager failed to find permission info for permission: " + checkedPermission);
            return false;
        }
        return true;
    }

    private String getCheckedPermissionFromArgs(Object[] args) {
        // in the check permission functions, the first string is always the permission to check (c.f. XPermissions)
        for (Object o : args) {
            if (o instanceof String) {
                return (String) o;
            }
        }
        return null;  // this will never happen.
    }

    private synchronized void addToThreadIdToPermissionDialogDataMap(PermissionDialogData permissionDialogData) {
        synchronized (this) {
            threadIdToPermissionDialogDataMap.put(permissionDialogData.threadId, permissionDialogData);
        }
    }

    private synchronized PermissionDialogData rmFromThreadIdToPermissionDialogDataMap(int threadId) {
        synchronized (this) {
            return threadIdToPermissionDialogDataMap.remove(threadId);
        }
    }

    private class IncomingHandler extends Handler {
        IncomingHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == PermissionManagerService.MSG_PROMPT_COMPLETE || msg.what == PermissionManagerService.MSG_PROMPT_DISMISSED) {
                XposedLogger.logDebug(TAG, packageName, "Received prompt complete");
                PermissionDialogData permissionDialogData = PermissionDialogData.fromBundle((Bundle) msg.obj);
                addToThreadIdToPermissionDialogDataMap(permissionDialogData);

                // unlock the starting thread
                CountDownLatch dialogCountDownLatch = XServiceHelper.getLatchFromWaitingThreadId(permissionDialogData.threadId);
                if (dialogCountDownLatch == null) {
                    XposedLogger.logError(TAG, packageName, "Failed to unlock thread with id " + permissionDialogData.threadId + "  after dialog response. Couldn't find the latch -- bug...");
                    return;
                }
                dialogCountDownLatch.countDown();
                XServiceHelper.rmThreadWaitingServiceResponse(permissionDialogData.threadId);
            } else {
                // ignore message
                XposedLogger.logWarn(TAG, packageName, "Receive unknown message from service. Bug?");
            }
        }
    }

    String handle(String checkedPermission, String checkedPermissionGroup, XC_MethodHook.MethodHookParam param) {
        XposedLogger.logDebug(TAG, packageName, "Starting to handle " + checkedPermission);

        // setup the necessary data to be sent to the server
        int checkingPid = Process.myPid();
        int threadId = Process.myTid();
        RunningApplicationInfo checkingApplicationInfo = new RunningApplicationInfo(packageName, null, checkingPid, applicationContext);
        RunningApplicationInfo ipcCallingApplicationInfo = getIpcCallingApplicationInfo(checkingPid);
        PermissionDialogData permissionDialogData = new PermissionDialogData(checkingApplicationInfo, ipcCallingApplicationInfo, checkedPermission, checkedPermissionGroup, param.method.getName(), threadId);

        if (!XServiceHelper.bound) {
            if (!XServiceHelper.isBinding) {
                XposedLogger.logWarn(TAG, applicationContext.getPackageName(), "Service is not bound but is also not binding. Is the service alive? Attempting to re-bind...");
                XServiceHelper.bindPermissionManagerService(applicationContext);
            }

            if (Looper.myLooper() == Looper.getMainLooper()) {
                // We can't lock the main thread or the onServiceConnected is never called and the thread hangs.
                // todo: we should find a way to bind the service at the start of the application, so it would be ready here.
                // alternatively, we could log the requests that we failed to handle and potentially ask later on.
                permissionDialogData.setUnhandled();
                XServiceHelper.addUnhandledPermission(permissionDialogData);
                return "Service is still binding but this is the main thread and we can't lock it. Logging unhandled permissiond and returning prematurely.";
            } else {
                // Still binding and we are not in the main thread, so we can wait for binding to complete.
                boolean boundUnlocked;
                try {
                    boundUnlocked = XServiceHelper.awaitBind(bindingTimeoutSeconds);
                } catch (InterruptedException e) {
                    return "Interrupted while attempting to bing with service. Returning prematurely. Error: " + e;
                }

                if (!boundUnlocked) {
                    return "Binding timed out.";
                }
            }
        }

        XCachedPermissionResult cachedResult = getCachedResult(checkedPermission, checkedPermissionGroup);
        if (cachedResult != null) {
            if (cachedResult.stillValid()) {
                XposedLogger.logDebug(TAG, packageName, "Valid permission check response still in cache. Returning cached result: " + cachedResult.toString());
                // we still send the result to the service, such that it stores the request
                permissionDialogData.grantResult = cachedResult.grantResult;
                permissionDialogData.answerType = PermissionDialogAnswerType.CACHE_ANSWERED;
                Message msg = Message.obtain(null, PermissionManagerService.MSG_CACHED_RESPONSE, permissionDialogData.toBundle());
                Exception error = XServiceHelper.trySendingMessage(msg);
                if (error != null) {
                    return "Error sending message: " + error.toString();
                }

                // because we can't overrule the system permission manager, we only set the result when it is denied
                if (permissionDialogData.grantResult == PackageManager.PERMISSION_DENIED)
                    param.setResult(permissionDialogData.grantResult);
                return null;
            } else {
                removeCachedResult(checkedPermission, checkedPermissionGroup);
            }
        }

        Message msg = Message.obtain(null, PermissionManagerService.MSG_NEW_PROMPT_CLIENT, permissionDialogData.toBundle());
        msg.replyTo = npmServiceInMessenger;
        Exception error = XServiceHelper.trySendingMessage(msg);
        if (error != null) {
            return "Error sending message: " + error.toString();
        }

        CountDownLatch dialogCountDownLatch = new CountDownLatch(1);
        XServiceHelper.addThreadWaitingServiceResponse(threadId, dialogCountDownLatch);
        // boolean dialogLatchReachedZero;
        try {
            XposedLogger.logDebug(TAG, packageName, "Thread with id " + threadId + " locking in latch while waiting for dialog response");
            // dialogLatchReachedZero = dialogCountDownLatch.await(PermissionDialogFragment.TIMEOUT_SECONDS + 10, TimeUnit.SECONDS);  // wait on dialog resolution
            dialogCountDownLatch.await();
            // there's no need for a timeout because if there's a problem, an ANR will occur.
            // Additionally, while the prompt is set for a 40s interval, it only counts when it is foreground. So it can exist for a longer period. So we wait as necessary
        } catch (InterruptedException e) {
            return "Interrupted while waiting for service response. Returning prematurely. Error: " + e;
        }

//        if (!dialogLatchReachedZero) {
//            XposedLogger.logError(TAG, packageName, "Handler thread " + handlerThread.getName() + " timed-out while waiting for dialog response. Exiting prematurely.");
//            // This should never happen, so we treat it as an error. This means that communication failed somewhere and the process hanged
//            return;
//        }

        // if it reaches here, we have a result from the dialog
        PermissionDialogData resultPermissionDialogData = rmFromThreadIdToPermissionDialogDataMap(threadId);
        if (resultPermissionDialogData == null) {
            // the thread was unlocked but we have no result. This shouldn't happen
            return "Latch unlocked but we got no result. Maybe the service disconnected. Exiting...";
        }

        // if it reaches here, resultPermissionDialogData is populated
        XposedLogger.logDebug(TAG, packageName, "Latch unlocked, got dialog answer type from service = " + resultPermissionDialogData.answerType);
        if (resultPermissionDialogData.answerType == PermissionDialogAnswerType.DISMISSED
                || resultPermissionDialogData.answerType == PermissionDialogAnswerType.TIMEDOUT) {
            // The prompt was dismissed or timedout, so we have no result
            return "Received dialog dismissed/timedout from service. Exiting hook";
        }

        // Put result in cache
        addCachedResult(resultPermissionDialogData.checkedPermission, resultPermissionDialogData.checkedPermissionGroup,
                new XCachedPermissionResult(resultPermissionDialogData.checkedPermission,
                        resultPermissionDialogData.timestamp,
                        resultPermissionDialogData.grantResult));

        // because we can't overrule the system permission manager, we only set the result when it is denied
        if (permissionDialogData.grantResult == PackageManager.PERMISSION_DENIED)
            param.setResult(resultPermissionDialogData.grantResult);

        return null;
    }

    private synchronized CountDownLatch getPermissionGroupResolutionLatch(String checkedPermissionGroup) {
        CountDownLatch latch = permissionGroupToResolutionLatch.get(checkedPermissionGroup);
        if (latch == null) {
            latch = new CountDownLatch(1);
            permissionGroupToResolutionLatch.put(checkedPermissionGroup, latch);
        }
        return latch;
    }

    private synchronized boolean isBeingHandled(String checkedPermissionGroup, int currentThreadId) {
        Integer handlingThreadId = permissionGroupToHandlerThreadIdMap.get(checkedPermissionGroup);
        if (handlingThreadId == null) {
            permissionGroupToHandlerThreadIdMap.put(checkedPermissionGroup, currentThreadId);  // This thread will handle it
            return false;
        }
        return true;
    }

    private synchronized void unlockThreadsWaitingForPermissionGroupResolution(String checkedPermissionGroup) {
        permissionGroupToHandlerThreadIdMap.remove(checkedPermissionGroup);
        CountDownLatch latch = permissionGroupToResolutionLatch.remove(checkedPermissionGroup);
        if (latch != null) {
            XposedLogger.logDebug(TAG, packageName, "Unlocking threads checking " + checkedPermissionGroup);
            latch.countDown();
        }
    }

    // We make it public such that is accessible to XRequestPermissionsHookHandler
    public static synchronized XCachedPermissionResult getCachedResult(@NonNull String checkedPermission, @Nullable String checkedPermissionGroup) {
        return checkedPermissionGroup != null ? permissionGroupCheckCacheMap.get(checkedPermissionGroup) : permissionGroupCheckCacheMap.get(checkedPermission);
    }

    public static synchronized void addCachedResult(@NonNull String checkedPermission, @Nullable String checkedPermissionGroup, @NonNull XCachedPermissionResult xCachedPermissionResult) {
        if (checkedPermissionGroup != null)
            permissionGroupCheckCacheMap.put(checkedPermissionGroup, xCachedPermissionResult);
        else
            permissionGroupCheckCacheMap.put(checkedPermission, xCachedPermissionResult);
    }

    // We make it public such that is accessible to XRequestPermissionsHookHandler
    public static synchronized XCachedPermissionResult removeCachedResult(@NonNull String checkedPermission, @Nullable String checkedPermissionGroup) {
        if (checkedPermissionGroup != null)
            return permissionGroupCheckCacheMap.remove(checkedPermissionGroup);
        else
            return permissionGroupCheckCacheMap.remove(checkedPermission);
    }

    @Nullable
    private RunningApplicationInfo getIpcCallingApplicationInfo(int checkingPid) {
        int callingPid = Binder.getCallingPid();
        if (callingPid == checkingPid)  // this is no ipc situation
            return null;

        XposedLogger.logDebug(TAG, packageName, "Handling a ipc situation");
        // we are handling a ipc situation
        int callingUid = Binder.getCallingUid();
        String[] uidPackages = applicationContext.getPackageManager().getPackagesForUid(callingUid);
        if (uidPackages == null || uidPackages.length == 0) {  // just for safety, but should't happen
            XposedLogger.logError(TAG, packageName, "Failed to get the package name for calling uid = " + callingUid);
            return null;
        }
        String callingPackageName = uidPackages[0];
        if (uidPackages.length > 1) {  // multiple packages may share uid
            try {
                java.lang.Process process = Runtime.getRuntime().exec("cat /proc/" + callingUid + "/cmdline");
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String cmdOutput = reader.readLine();
                reader.close();

                // Waits for the command to finish.
                process.waitFor();
                if (cmdOutput.length() > 0) {
                    // because the file does not have a newline, we remove the null chars.
                    callingPackageName = cmdOutput.replace("\0", "");
                }
            } catch (IOException | InterruptedException e) {
                XposedLogger.logDebug(TAG, packageName, "Failed to get callingPackageName. Using the most likely value.");
                XposedLogger.logException(e);
            }
        }
        return new RunningApplicationInfo(callingPackageName, null, callingPid, applicationContext);
    }
}
