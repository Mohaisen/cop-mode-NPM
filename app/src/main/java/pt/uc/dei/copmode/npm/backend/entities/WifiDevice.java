package pt.uc.dei.copmode.npm.backend.entities;

import android.net.wifi.ScanResult;
import android.os.SystemClock;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class WifiDevice {
    public String ssid;
    public String bssid;
    public int rssid;  // aka level
    public long timestamp;
    public long time;


    public WifiDevice(String ssid, String bssid, int rssid, long timestamp, long time) {
        this.ssid = ssid;
        this.bssid = bssid;
        this.rssid = rssid;
        this.timestamp = timestamp;
        this.time = time;
    }

    public WifiDevice(ScanResult scanResult) {
        this.ssid = scanResult.SSID;
        this.bssid = scanResult.BSSID;
        this.rssid = scanResult.level;
        this.timestamp = scanResult.timestamp;
        this.time =  System.currentTimeMillis() - SystemClock.elapsedRealtime() + Math.round(timestamp*Math.pow(10, -3));
    }

    public static List<WifiDevice> fromScanResultsList(List<ScanResult> scanResults) {
        List<WifiDevice> newListDevices = new ArrayList<>(scanResults.size());
        for (ScanResult result: scanResults) {
            newListDevices.add(new WifiDevice(result));
        }
        return newListDevices;
    }

    @NonNull
    @Override
    public String toString() {
        return "WifiDevice(ssid: " + ssid + ", bssid: " + bssid + ", rssid: " + rssid + ", timestamp: " + timestamp + ", time: " + time + ")";
    }
}
