package pt.uc.dei.copmode.npm.xposed;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import pt.uc.dei.copmode.npm.BuildConfig;


public class XPermissions implements IXposedHookLoadPackage {
    private static final String TAG = "xposed/XPermissions";
    private final List<XHook> permissionHooks = initializeHooks();

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        // MyLogHelper.logDebug(TAG, "Loaded: " + lpparam.packageName);
        // AndroidAppHelper.currentApplicationInfo().uid
        // apps have uid between 10000 and 19999.

        if (!"android".equals(lpparam.packageName) && !lpparam.packageName.equals(BuildConfig.APPLICATION_ID)) {
            if (lpparam.appInfo.targetSdkVersion >= Build.VERSION_CODES.M) {
                // hook only runtime permissioned apps to avoid runtime problems
                if ((lpparam.appInfo.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) == 0 ||
                        lpparam.packageName.equals("pt.uc.dei.copmode.npmtester")) {  // for the test app
                    // non system apps only
                    XposedLogger.logDebug(null, TAG, "Setting hooks in " + lpparam.packageName);
                    for (XHook hook: permissionHooks) {
                        hook.hook(lpparam);
                    }
                }
            }
        }

        // use for testing
//        if (lpparam.packageName.equals("pt.uc.dei.copmode.npmtester")) {
//            XposedLogger.logDebug(null, TAG, "Setting hooks in " + lpparam.packageName);
//            for (XHook hook: permissionHooks) {
//                hook.hook(lpparam);
//            }
//        }
    }

    private List<XHook> initializeHooks() {
        ArrayList<XHook> permissionHooks = new ArrayList<>();  // todo set capacity?
        addApplicationOnCreateHook(permissionHooks);
        addCheckPermissionHooks(permissionHooks);
        addRequestPermissionHooks(permissionHooks);
        return permissionHooks;
    }

    private void addApplicationOnCreateHook(ArrayList<XHook> permissionHooks) {
        XC_MethodHook xApplicationOnCreateHookHandle = new XApplicationOnCreateHookHandle();
        permissionHooks.add(new XHook("android.app.Application", "onCreate", xApplicationOnCreateHookHandle));
    }

    private void addCheckPermissionHooks(ArrayList<XHook> permissionHooks) {
        XC_MethodHook returnDenyOrDenyAppOpHandle = new XCheckPermissionHookHandler();

        // We leave URI permissions untouched as these are not directly related to permissions, but rather with data exchange
        // for checkPermission(String,int,int) function, we are not parsing the pid and uid as this is used to check if a process within the application has the permission

        // https://developer.android.com/reference/androidx/core/content/ContextCompat#checkSelfPermission(android.content.Context,%20java.lang.String)
        permissionHooks.add(new XHook("androidx.core.content.ContextCompat", "checkSelfPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class));

        // https://developer.android.com/reference/android/support/v4/content/ContextCompat#checkselfpermission
        permissionHooks.add(new XHook("android.support.v4.content.ContextCompat", "checkSelfPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class));

        // https://developer.android.com/reference/android/content/ContextWrapper -- used by most activities
        permissionHooks.add(new XHook("android.content.ContextWrapper","checkCallingOrSelfPermission", returnDenyOrDenyAppOpHandle, String.class));
        permissionHooks.add(new XHook("android.content.ContextWrapper","checkCallingPermission", returnDenyOrDenyAppOpHandle, String.class));
        permissionHooks.add(new XHook("android.content.ContextWrapper","checkPermission", returnDenyOrDenyAppOpHandle, String.class, "int", "int"));
        permissionHooks.add(new XHook("android.content.ContextWrapper","checkSelfPermission", returnDenyOrDenyAppOpHandle, String.class));

        // https://android.googlesource.com/platform/frameworks/base/+/master/core/java/android/app/ContextImpl.java#1863
        // Common implementation of Context API, which provides the base. Provides the base context object for Activity and other application components.
        permissionHooks.add(new XHook("android.app.ContextImpl","checkCallingOrSelfPermission", returnDenyOrDenyAppOpHandle, String.class));
        permissionHooks.add(new XHook("android.app.ContextImpl","checkCallingPermission", returnDenyOrDenyAppOpHandle, String.class));
        permissionHooks.add(new XHook("android.app.ContextImpl","checkPermission", returnDenyOrDenyAppOpHandle, String.class, "int", "int"));
        // not app accessible. It will be hooked either upstream or downstream
        // permissionHooks.add(new XHook("android.app.ContextImpl","checkPermission", returnDenyOrDenyAppOpHandle, String.class, "int", "int", IBinder.class));
        permissionHooks.add(new XHook("android.app.ContextImpl","checkSelfPermission", returnDenyOrDenyAppOpHandle, String.class));

        // https://developer.android.com/reference/androidx/core/content/PermissionChecker
        permissionHooks.add(new XHook("androidx.core.content.PermissionChecker","checkCallingOrSelfPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class));
        permissionHooks.add(new XHook("androidx.core.content.PermissionChecker","checkCallingPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class, String.class));
        permissionHooks.add(new XHook("androidx.core.content.PermissionChecker","checkPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class, "int", "int", String.class));
        permissionHooks.add(new XHook("androidx.core.content.PermissionChecker","checkSelfPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class));

        // https://developer.android.com/reference/android/support/v4/content/PermissionChecker
        permissionHooks.add(new XHook("android.support.v4.content.PermissionChecker","checkCallingOrSelfPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class));
        permissionHooks.add(new XHook("android.support.v4.content.PermissionChecker","checkCallingPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class, String.class));
        permissionHooks.add(new XHook("android.support.v4.content.PermissionChecker","checkPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class, "int", "int", String.class));
        permissionHooks.add(new XHook("android.support.v4.content.PermissionChecker","checkSelfPermission", returnDenyOrDenyAppOpHandle, Context.class, String.class));

        // https://developer.android.com/reference/android/content/pm/PackageManager#checkPermission(java.lang.String,%20java.lang.String)
        // This method is abstract and I'm having an hard time finding the implementations. I doubt apps use the package manager to check if they have a certain permission.
        // Anyway, these are the implementations I've found:
        // ApplicationPackageManager, which uses ContextImpl (hooked above) to check permissions (internally uses IActivityManager which I couldn't find the implementation)
        // PackageManagerService which calls PermissionManagerService methods
        // https://android.googlesource.com/platform/frameworks/base/+/master/services/core/java/com/android/server/pm/PackageManagerService.java
        // https://android.googlesource.com/platform/frameworks/base/+/master/services/core/java/com/android/server/pm/permission/PermissionManagerService.java
        // TODO: int parameters are callingUid and userId, so these need a specific handler. Maybe I just look at permName and packageName
        // permissionHooks.add(new XHook("com.android.server.pm.permission.PermissionManagerService","checkPermission", returnDenyOrDenyAppOpHandle, String.class, String.class, "int", "int"));
        // permissionHooks.add(new XHook("com.android.server.pm.permission.checkUidPermission","checkPermission", returnDenyOrDenyAppOpHandle, String.class, "android.content.pm.PackageParser.Package", "int", "int"));
    }

    private void addRequestPermissionHooks(ArrayList<XHook> permissionHooks) {
        XC_MethodHook requestPermissionsHookHandler = new XRequestPermissionsHookHandler();
        // some parameters written in string to avoid confusion between main classes are they compat counter-parts

        // https://developer.android.com/reference/android/app/Activity#requestPermissions(java.lang.String%5B%5D,%20int)
        permissionHooks.add(new XHook("android.app.Activity", "requestPermissions", requestPermissionsHookHandler, String[].class, "int"));
        // results sent to onRequestPermissionsResult(int, java.lang.String[], int[])

        // https://developer.android.com/reference/androidx/core/app/ActivityCompat#requestPermissions(android.app.Activity,%20java.lang.String%5B%5D,%20int)
        permissionHooks.add(new XHook("androidx.core.app.ActivityCompat", "requestPermissions", requestPermissionsHookHandler, "android.app.Activity", String[].class, "int"));

        // https://developer.android.com/reference/android/support/v4/app/ActivityCompat#requestpermissions
        permissionHooks.add(new XHook("android.support.v4.app.ActivityCompat", "requestPermissions", requestPermissionsHookHandler, "android.app.Activity", String[].class, "int"));
        // results sent to onRequestPermissionsResult(int, String[], int[])

        // https://developer.android.com/reference/android/app/Fragment#requestPermissions(java.lang.String[],%20int)
        permissionHooks.add(new XHook("android.app.Fragment", "requestPermissions", requestPermissionsHookHandler, String[].class, "int"));
        // results sent to onRequestPermissionsResult(int, String[], int[])

        // https://developer.android.com/reference/androidx/fragment/app/Fragment#requestPermissions(java.lang.String%5B%5D,%20int)
        permissionHooks.add(new XHook("androidx.fragment.app.Fragment", "requestPermissions", requestPermissionsHookHandler, String[].class, "int"));

        // https://developer.android.com/reference/android/support/v4/app/Fragment#requestPermissions(java.lang.String[],%20int)
        permissionHooks.add(new XHook("android.support.v4.app.Fragment", "requestPermissions", requestPermissionsHookHandler, String[].class, "int"));
        // results sent to onRequestPermissionsResult(int, String[], int[])

        // https://developer.android.com/reference/android/support/v13/app/FragmentCompat#requestPermissions(android.app.Fragment,%20java.lang.String[],%20int)
        permissionHooks.add(new XHook("android.support.v13.app.FragmentCompat", "requestPermissions", requestPermissionsHookHandler, "android.app.Fragment", String[].class, "int"));
        // results sent to onRequestPermissionsResult(int, String[], int[])

        // https://developer.android.com/reference/androidx/test/runner/permission/PermissionRequester#requestpermissions
        // This is only for tests
    }

}