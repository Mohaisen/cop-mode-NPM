package pt.uc.dei.copmode.npm.xposed;

import android.app.Application;

import de.robv.android.xposed.XC_MethodHook;

/**
 * This class handles the hook to Application.onCreate(), in where we start binding the application
 * to the permission manager service.
 */
public class XApplicationOnCreateHookHandle extends XC_MethodHook {
    private final static String TAG = "xposed/XApplicationOnCreateHookHandle";

    @Override
    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
        Application app = (Application) param.thisObject;
        XposedLogger.logDebug(TAG, app.getPackageName(), "Application starting. Binding to npm service.");
        XServiceHelper.bindPermissionManagerService(app.getApplicationContext());
    }
}
