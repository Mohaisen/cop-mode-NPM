package pt.uc.dei.copmode.npm.backend.entities;

import android.bluetooth.BluetoothDevice;

public class MyBluetoothDevice {
    public String name;
    public String address;
    public int type;
    public int bondState;
    public int deviceClass;
    public short rssi;
    public long time;

    public MyBluetoothDevice(String name, String address, int type, int bondState, int deviceClass, short rssi, long time) {
        this.name = name;
        this.address = address;
        this.type = type;
        this.bondState = bondState;
        this.deviceClass = deviceClass;
        this.rssi = rssi;
        this.time = time;
    }

    public static MyBluetoothDevice fromBluetoothDevice(BluetoothDevice device, short rssi, long time) {
        return new MyBluetoothDevice(device.getName(), device.getAddress(), device.getType(),
                device.getBondState(), device.getBluetoothClass().getDeviceClass(), rssi, time);
    }
}
