package pt.uc.dei.copmode.npm.frontend.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.frontend.adapters.PermissionRequestsViewAdapter;
import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;
import pt.uc.dei.copmode.npm.frontend.viewmodels.MainActivityViewModel;

public class MainActivity extends AppCompatActivity {
    private static String TAG = "MainActivity";

    private AlertDialog helpDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setTitle(R.string.mainactivity_actionbar_title);
        }

        PermissionManagerService.startServiceIfOff(this);

        MainActivityViewModel viewModel = MainActivityViewModel.fromProvider(this, NPMDatabase.getDB(this).permissionDialogDataDao());

        RecyclerView recyclerView = findViewById(R.id.permission_requests_history);
        final PermissionRequestsViewAdapter adapter = new PermissionRequestsViewAdapter();
        viewModel.permissionDialogDataList.observe(this, new Observer<List<PermissionDialogData>>() {
            @Override
            public void onChanged(List<PermissionDialogData> list) {
                adapter.submitList(list);
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        createHelpDialog(this);
        ImageView helpImageView = findViewById(R.id.help_image_view);
        helpImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helpDialog.show();
            }
        });

    }

    private void createHelpDialog(Activity activity) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = activity.getLayoutInflater().inflate(R.layout.help_dialog, null);
        dialogView.findViewById(R.id.close_help_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helpDialog.dismiss();
            }
        });
        dialogBuilder.setView(dialogView);
        helpDialog = dialogBuilder.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_item_uploads:
                intent = new Intent(this, UploadsActivity.class);
                break;
            case R.id.menu_item_statistics:
                intent = new Intent(this, StatisticsActivity.class);
                break;
            case R.id.menu_item_about:
                intent = new Intent(this, AboutActivity.class);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        startActivity(intent);
        return true;
    }


}
