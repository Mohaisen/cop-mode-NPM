package pt.uc.dei.copmode.npm.receivers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pt.uc.dei.copmode.npm.backend.entities.MyBluetoothDevice;
import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

/**
 * Even though there's an option to always scan bluetooth devices even when bluetooth is off (just as with wi-fi),
 * unlike wifi, there's no way to redirect the user to such setting.
 */
public class BluetoothReceiver extends BroadcastReceiver implements ContextReceiver {
    private static final String TAG = "BluetoothReceiver";
    private boolean isReceiving = false;

    private static final long DISCOVERY_PERIOD_MILLIS = 300000;  // scan every 5 minutes. We also get results from other apps' scans

    private static final List<MyBluetoothDevice> deviceList = new ArrayList<>();
    private static long discoveryStartTime;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return;

        switch (action) {
            case BluetoothDevice.ACTION_FOUND:
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device == null)
                    return;  // this should never happen

                // MyLogHelper.logDebug(TAG, "Found bluetooth device: " + device);
                short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                deviceList.add(MyBluetoothDevice.fromBluetoothDevice(device, rssi, discoveryStartTime));
                break;
            case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                MyLogHelper.logDebug(TAG, "New bluetooth discovery starting");
                discoveryStartTime = System.currentTimeMillis();
                deviceList.clear(); // clean list when scan starts

                break;
            case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                MyLogHelper.logDebug(TAG, "Bluetooth discovery finished");
                ContextualDataHolder.getInstance(context).updateBluetoothDevices(deviceList);
                break;
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                if (bluetoothState == BluetoothAdapter.STATE_ON) {
                    // Bluetooth just went on, so we do a discovery instead of waiting for the next timer
                    MyLogHelper.logDebug(TAG, "Bluetooth turned on. Starting discovery");
                    BluetoothAdapter.getDefaultAdapter().startDiscovery();
                }
                break;
        }
    }

    @Override
    public boolean registerSelf(Context context) {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            isReceiving = true;
            return true;  // even though this fails to register, we return true because it fails due to hardware limitations
        }

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(this, filter);

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                boolean success = bluetoothAdapter.startDiscovery();
                if (!success)
                    MyLogHelper.logDebug(TAG, "Failed to start bluetooth discovery");
                // fails to start if bluetooth is off, which is fine
            }
        };
        timer.schedule(timerTask, 0, DISCOVERY_PERIOD_MILLIS);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }
}
