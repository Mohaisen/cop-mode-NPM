package pt.uc.dei.copmode.npm.helpers;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;

import pt.uc.dei.copmode.npm.components.Uploader;
import pt.uc.dei.copmode.npm.services.ActivityCheckService;
import pt.uc.dei.copmode.npm.services.SetupCheckService;

public class JobSchedulerHelper {
    private static final String TAG = "JobSchedulerHelper";

    // Setup check service
    private static final int PERIODIC_SETUP_CHECK_JOB_ID = 110110;
    private static final long PERIODIC_SETUP_CHECK_PERIODICITY_MILLIS = 3600000L;  // every hour

    // Activity check service
    private static final int PERIODIC_ACTIVITY_CHECK_JOB_ID = 110111;
    private static final long PERIODIC_ACTIVITY_CHECK_PERIODICITY_MILLIS = 3600000L;

    // start periodic jobs
    public static void startPeriodicJobs(Context applicationContext) {
        Uploader.getInstance(applicationContext);  // start the uploader component
        schedulePeriodicService(applicationContext, SetupCheckService.class, PERIODIC_SETUP_CHECK_JOB_ID, PERIODIC_SETUP_CHECK_PERIODICITY_MILLIS);
        schedulePeriodicService(applicationContext, ActivityCheckService.class, PERIODIC_ACTIVITY_CHECK_JOB_ID, PERIODIC_ACTIVITY_CHECK_PERIODICITY_MILLIS);
        // ...
    }

    private static void schedulePeriodicService(Context context, Class<? extends JobService> service, int jobId, long periodocityMillis) {
        // Because the user can at any time disable some of npm permissions, we have to schedule a periodic infinite check
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (scheduler == null) {
            // This should'nt happen so there's no need to better handle this
            MyLogHelper.logError(TAG, "Failed to get job scheduler system service...");
            return;
        }

        int status = scheduler.schedule(new JobInfo.Builder(jobId,
                new ComponentName(context, service))
                .setPeriodic(periodocityMillis)
                .build());
        if (status == JobScheduler.RESULT_FAILURE) {
            MyLogHelper.logError(TAG, "Failed to schedule " + service.getSimpleName());
        }
    }
}
