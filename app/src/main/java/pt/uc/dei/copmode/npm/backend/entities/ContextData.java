package pt.uc.dei.copmode.npm.backend.entities;

import android.os.SystemClock;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;
import java.util.List;

import pt.uc.dei.copmode.npm.backend.converters.ListCalendarEventsConverter;
import pt.uc.dei.copmode.npm.backend.converters.ListMyBluetoothDevicesConverter;
import pt.uc.dei.copmode.npm.backend.converters.ListWifiDevicesConverter;
import pt.uc.dei.copmode.npm.backend.converters.NetworkStatusConverter;
import pt.uc.dei.copmode.npm.backend.database.ContextDataDao;

/**
 * ContextData stores the device context data at the time of a permission prompt/dialog.
 * Thus, there's a relation one-to-one, where PermissionDialogData has a ContextData.
 *
 * This class could've been embedded in PermissionDialogData. However, since the contextual data
 * does not need to be sent back and forth between the service and requesting applications, we separate
 * it, such that it only lives in the service (or in the NPM application). Thus, a ContextData
 * should only be created after saving the corresponding PermissionDialogData.
 */
@Entity(tableName = ContextDataDao.CONTEXT_DATA_TABLE)
public class ContextData {
    @PrimaryKey(autoGenerate = true)
    private long contextDataId;
    public long getContextDataId() {
        return contextDataId;
    }

    public long permissionDialogDataOwnerId;

    @Embedded(prefix = "top_")
    public RunningApplicationInfo topRunningApplicationInfo;

    // @Embedded(prefix = "foreground_")
    public RunningApplicationInfo[] foregroundRunningApplicationInfoArray;

    // @Embedded(prefix = "background_")
    public RunningApplicationInfo[] backgroundRunningApplicationInfoArray;

    @TypeConverters(NetworkStatusConverter.class)
    public NetworkStatus networkStatus = NetworkStatus.DISCONNECTED;

    public boolean screenIsInteractive;
    public boolean isKeyguardLocked;
    public int dockState;
    public int callState;  // TelephonyManager.CALL_STATE_IDLE, TelephonyManager.CALL_STATE_RINGING, or TelephonyManager.CALL_STATE_OFFHOOK
    public int plugState;

    @Embedded
    public LocationData location;

    @TypeConverters(ListWifiDevicesConverter.class)
    public List<WifiDevice> wifiDevices;
    @TypeConverters(ListMyBluetoothDevicesConverter.class)
    public List<MyBluetoothDevice> bluetoothDevices;

    @TypeConverters(ListCalendarEventsConverter.class)
    public List<CalendarEvent> calendarEvents;

    public final long bootTime;

    @Ignore
    public ContextData() {
        bootTime = System.currentTimeMillis() - SystemClock.elapsedRealtime();
    }

    public ContextData(long contextDataId, long bootTime) {
        this.contextDataId = contextDataId;
        this.bootTime = bootTime;
    }

    @Ignore
    public ContextData(ContextData original) {
        this.contextDataId = original.contextDataId;
        this.bootTime = original.bootTime;
        this.permissionDialogDataOwnerId = original.permissionDialogDataOwnerId;
        if (original.topRunningApplicationInfo != null)
            this.topRunningApplicationInfo = new RunningApplicationInfo(original.topRunningApplicationInfo);
        if (original.foregroundRunningApplicationInfoArray != null)
            this.foregroundRunningApplicationInfoArray = original.foregroundRunningApplicationInfoArray.clone();
        if (original.backgroundRunningApplicationInfoArray != null)
            this.backgroundRunningApplicationInfoArray = original.backgroundRunningApplicationInfoArray.clone();
        this.networkStatus = NetworkStatusConverter.toNetworkStatus(original.networkStatus.getStatus());  // to copy
        this.screenIsInteractive = original.screenIsInteractive;
        this.isKeyguardLocked = original.isKeyguardLocked;
        this.dockState = original.dockState;
        this.callState = original.callState;
        this.plugState = original.plugState;
        this.location = original.location;
        if (original.wifiDevices != null)
            this.wifiDevices = new ArrayList<>(original.wifiDevices);
        if (original.bluetoothDevices != null)
            this.bluetoothDevices = new ArrayList<>(original.bluetoothDevices);
    }
}
