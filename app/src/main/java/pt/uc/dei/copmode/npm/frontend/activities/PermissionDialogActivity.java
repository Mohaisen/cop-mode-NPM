package pt.uc.dei.copmode.npm.frontend.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.frontend.fragments.PermissionDialogFragment;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

public class PermissionDialogActivity extends AppCompatActivity implements PermissionDialogFragment.PermissionDialogListener {
    private static final String TAG = "PermissionDialogAct";
    private static final String DIALOG_FRAGMENT_TAG = "PermissionDialogFragment";

    Messenger mService = null;
    boolean bound;

    PermissionDialogData permissionDialogData;
    private boolean promptTerminatedNormally = false;  // either through user answer or timeout

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            bound = true;
            MyLogHelper.logDebug(TAG, "bound");
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            bound = false;
            MyLogHelper.logDebug(TAG, "Service disconnected");
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.hide();
        if (savedInstanceState == null) {
            Bundle bundledData = getIntent().getBundleExtra(PermissionDialogData.INTENT_KEY);
            permissionDialogData = PermissionDialogData.fromBundle(Objects.requireNonNull(bundledData));
            DialogFragment newFragment = new PermissionDialogFragment(permissionDialogData);
            newFragment.show(getSupportFragmentManager(), DIALOG_FRAGMENT_TAG);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindPermissionManagerService();
    }

    private void bindPermissionManagerService() {
        if (!bound)  // because we only unbind in onDestroy, we might attempt to bind multiple times in onStart
            bindService(new Intent(this, PermissionManagerService.class), mConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDialogConfirmClick(PermissionDialogData permissionDialogData) {
        MyLogHelper.logDebug(TAG, "User answered permission dialog. Sending to service.");
        permissionDialogData.answerType = PermissionDialogAnswerType.USER_ANSWERED;
        attemptToSendDataAndFinish(permissionDialogData);
    }

    @Override
    public void onDialogTimeout(PermissionDialogData permissionDialogData) {
        MyLogHelper.logDebug(TAG, "Dialog Timed-out. Sending to service");
        permissionDialogData.answerType = PermissionDialogAnswerType.TIMEDOUT;
        attemptToSendDataAndFinish(permissionDialogData);
    }

    private void attemptToSendDataAndFinish(final PermissionDialogData permissionDialogData) {
        promptTerminatedNormally = true;
        String error = attemptToSendMessage(PermissionManagerService.MSG_PROMPT_COMPLETE, permissionDialogData);
        if (error != null) {
            MyLogHelper.logError(TAG, error);
        }
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);  // removes exit transition
    }

    @Override
    protected void onDestroy() {
        if (!promptTerminatedNormally) {
            // The user might dismiss the activity without finishing answering (through the overview screen)
            permissionDialogData.answerType = PermissionDialogAnswerType.DISMISSED;
            String error = attemptToSendMessage(PermissionManagerService.MSG_PROMPT_DISMISSED, permissionDialogData);
            if (error != null) {
                MyLogHelper.logError(TAG, error);
            }
        }

        // Unbind from the service
        if (bound) {
            unbindService(mConnection);
            bound = false;
        }
        super.onDestroy();
    }

    private String attemptToSendMessage(int messageIdentifier, PermissionDialogData data) {
        // todo: the user might promptly dismiss the dialog. So we should check whether it is still binding and wait a little
        if (bound) {
            Message msg = Message.obtain(null, messageIdentifier, data.toBundle());
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                return "Failed to send message to service: " + e;
            }
            return null;
        }
        return "Service was not boundnd when sending message with identifier " + messageIdentifier;
    }

}
