package pt.uc.dei.copmode.npm.backend.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import pt.uc.dei.copmode.npm.backend.entities.ContextData;

@Dao
public interface ContextDataDao {
    public final String CONTEXT_DATA_TABLE = "context_data_table";

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insertContextData(ContextData data);

    @Query("SELECT * FROM " + CONTEXT_DATA_TABLE)
    public ContextData[] getAllContextData();
}
