package pt.uc.dei.copmode.npm.components;

import android.app.AppOpsManager;
import android.app.KeyguardManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.wifi.ScanResult;
import android.os.BatteryManager;
import android.os.PowerManager;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

import pt.uc.dei.copmode.npm.backend.entities.CalendarEvent;
import pt.uc.dei.copmode.npm.backend.entities.ContextData;
import pt.uc.dei.copmode.npm.backend.entities.LocationData;
import pt.uc.dei.copmode.npm.backend.entities.MyBluetoothDevice;
import pt.uc.dei.copmode.npm.backend.entities.NetworkStatus;
import pt.uc.dei.copmode.npm.backend.entities.RunningApplicationInfo;
import pt.uc.dei.copmode.npm.backend.entities.WifiDevice;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

/**
 * This class handles receives the changes in the context (captured by the receivers)
 * Most of the data could potentially be collected in a sync way, without the need of the receivers.
 * However, that would result in a lost of performance, as for each permission request we would have
 * to gather the data.
 */
public class ContextualDataHolder {
    private static final String TAG = "ContextualDataCollector";

    private static volatile ContextualDataHolder instance;

    private final ContextData contextData;

    private List<CalendarEvent> calendarEventsCache;

    //    private List<UsageStats> usageStats = null;
    //    private UsageEvents events;
    //    private SortedMap<Long, UsageStats> sortedStats;

    private ContextualDataHolder(Context context) {
        contextData = new ContextData();

        // todo instead of initializing contextual data here, I should add a method in ContextReceiver to initialize the value of each receiver
        // === initialize contextData ===
        IntentFilter iFilter;  // for sticky broadcasts
        contextData.screenIsInteractive = ((PowerManager) context.getSystemService(Context.POWER_SERVICE)).isInteractive();
        contextData.isKeyguardLocked = ((KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE)).isKeyguardLocked();
        contextData.callState = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getCallState();

        // dock state -- sticky broadcast
        iFilter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
        Intent dockStatus = context.registerReceiver(null, iFilter);
        if (dockStatus != null) {
            contextData.dockState = dockStatus.getIntExtra(Intent.EXTRA_DOCK_STATE, -1);
        } else {
            // if it fails to get this sticky broadcast, we assume undocked state
            contextData.dockState = Intent.EXTRA_DOCK_STATE_UNDOCKED;
        }

        // plug state -- sticky broadcast
        iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent plugStateIntent = context.registerReceiver(null, iFilter);
        if (plugStateIntent != null) {
            contextData.plugState = plugStateIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        } else {
            contextData.plugState = -1;
        }

        // get networkStatus as the network might already be active.
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network activeNetwork = connMgr.getActiveNetwork();
        if (activeNetwork == null) {
            contextData.networkStatus = NetworkStatus.DISCONNECTED;
        } else {
            if (connMgr.isActiveNetworkMetered())
                contextData.networkStatus = NetworkStatus.METERED;
            else
                contextData.networkStatus = NetworkStatus.NOT_METERED;
        }
    }

    public static ContextualDataHolder getInstance(Context context) {
        if (instance == null) {
            synchronized (ContextualDataHolder.class) {
                if (instance == null) {
                    instance = new ContextualDataHolder(context);
                }
            }
        }
        return instance;
    }

    public ContextData getCurrentContextCopy(Context context) {
        ContextData copy = new ContextData(contextData);
        copy.calendarEvents = retrieveOnGoingCalendarEvents();
        RunningApplicationsCollector.populateRunningApplicationsInfo(copy, context);
        return copy;
    }

    @Deprecated  // in favor of RunningApplicationsCollector
    private RunningApplicationInfo getForegroundRunningAppInfo(Context context) {
        if (!contextData.screenIsInteractive) {
            return null;  // no application is actually in the foreground
        }

        if (!PermissionManagerService.hasUsageAccessPermission(context.getApplicationContext())) {
            return null;
        }

        final UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        SortedMap<Long, UsageStats> sortedStats = new TreeMap<>();
        if (usageStatsManager != null) {
            long currentTime = System.currentTimeMillis();
            // long bootTime = currentTime - elapsedRealtime();
            long beginTime = currentTime - 60000;  // 60 seconds ago

            List<UsageStats> usageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, beginTime, currentTime);
            // MyLogHelper.logDebug(TAG, "current time is " + currentTime + " usageStats size:" + usageStats.size());

            for (UsageStats appStats : usageStats) {
                Calendar lastTimeUsedCal = Calendar.getInstance();
                lastTimeUsedCal.setTimeInMillis(appStats.getLastTimeUsed());
                // MyLogHelper.logDebug(TAG, appStats.getPackageName() + " last time used: " + DateFormat.format("yyyy-MM-dd HH:mm:ss", lastTimeUsedCal).toString() + " foreground time " + appStats.getTotalTimeInForeground());
                sortedStats.put(appStats.getLastTimeUsed(), appStats);
            }
//                events = usageStatsManager.queryEvents(beginTime, currentTime);
//                MyLogHelper.logDebug(TAG, "Size of events: " + events.hasNextEvent());
//                while (events.hasNextEvent()) {
//                    UsageEvents.Event event = new UsageEvents.Event();
//                    events.getNextEvent(event);
//                    MyLogHelper.logDebug(TAG, event.getPackageName() + " " + event.getEventType());
//                }
        }
        if (sortedStats.isEmpty()) {
            return null;
        }

        String foregroundPackageName = Objects.requireNonNull(sortedStats.get(sortedStats.lastKey())).getPackageName();
        return new RunningApplicationInfo(foregroundPackageName, true, null, context);
    }


    public void updateNetwork(NetworkCapabilities networkCapabilities) {
        if (networkCapabilities == null || !networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET))
            contextData.networkStatus = NetworkStatus.DISCONNECTED;
        else {
            if(networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED))
                contextData.networkStatus =  NetworkStatus.NOT_METERED;
            else
                contextData.networkStatus =  NetworkStatus.METERED;
        }
    }

    public boolean hasNetworkConnection() {
        return contextData.networkStatus != NetworkStatus.DISCONNECTED;
    }

    public void updateScreenIsInteractive(boolean isInteractive) {
        contextData.screenIsInteractive = isInteractive;
    }

    public void updateIsKeyguardLocked(boolean isKeyguardLocked) {
        contextData.isKeyguardLocked = isKeyguardLocked;
    }

    public void updateDockState(int dockState) {
        contextData.dockState = dockState;
    }

    public void updateCallState(int state) {
        contextData.callState = state;
    }

    public void updatePlugState(int plugState) {
        contextData.plugState = plugState;
    }

    public void updateLocation(@NonNull Location newLocation) {
        contextData.location = new LocationData(newLocation);
    }

    public void updateWifiDevices(List<ScanResult> results) {
        contextData.wifiDevices = WifiDevice.fromScanResultsList(results);
    }

    public void updateBluetoothDevices(List<MyBluetoothDevice> deviceList) {
        contextData.bluetoothDevices = deviceList;
    }


    public void updateCalendarEventsCache(List<CalendarEvent> calendarEvents) {
        calendarEventsCache = calendarEvents;
    }

    private List<CalendarEvent> retrieveOnGoingCalendarEvents() {
        List<CalendarEvent> toReturnList = new ArrayList<>();
        long currentTime = System.currentTimeMillis();
        if(calendarEventsCache != null) {
            for (CalendarEvent calendarEvent: calendarEventsCache) {
                if (calendarEvent.eventBegin <= currentTime && currentTime <= calendarEvent.eventEnd)
                    toReturnList.add(calendarEvent);
            }
        }
        return toReturnList;
    }

    public boolean shouldPrompt() {
        return (contextData.screenIsInteractive && !contextData.isKeyguardLocked);
    }

    private static class RunningApplicationsCollector {
        private static final String TAG = "RunningApplicationsCollector";

        // Process info can be checked in services/java/com/android/server/am/ProcessList.java
        // and services/java/com/android/server/am/ActivityManagerService.java

        //  We can add -e 'PERS #' to get persistent services. But these are always running and are system level
        private static final String[] CMD_COMMAND = {"/bin/sh", "-c", "dumpsys activity | grep -e 'Proc #'"};

        // see function dumpProcessOomList in ActivityManagerService for details
        static void populateRunningApplicationsInfo(ContextData contextData, Context context) {
            if (!hasUsageAccessPermission(context)) {
                MyLogHelper.logError(TAG, "Failed to collect running applications due to lacking permission PACKAGE_USAGE_STATS");
            } else {
                String[] cmdOutput = getCmdOutput();
                HashMap<String, RunningApplicationInfo> foregroundApplications = new HashMap<>();
                HashMap <String, RunningApplicationInfo> backgroundApplications = new HashMap<>();
                RunningApplicationInfo topApplication;
                for (String process: cmdOutput != null ? cmdOutput : new String[0]) {
                    // MyLogHelper.logDebug(TAG, process.trim());

                    String cleanProcessLine = process.trim();  // the output has a variable amount of spaces, so we need to be careful with the indices

                    String oomAdjField = cleanProcessLine.split(":")[1].trim().split(" ")[0].trim();
                    // MyLogHelper.logDebug(TAG, "oomAdjField = " + oomAdjField);
                    if (isCached(oomAdjField))
                        continue;

                    String[] processFields = cleanProcessLine.split(" ");
                    String[] processRecordFields = processFields[processFields.length-2].split(":");
                    String packageName = processRecordFields[1].split("/")[0];  // this split removes the user

                    int pid = Integer.parseInt(processRecordFields[0]);

                    String isForegroundField = cleanProcessLine.split("/")[1];
                    boolean isForeground = isForeground(isForegroundField);

                    if (isForegroundField.equals("A")) {
                        // there can only be one foreground activity
                        contextData.topRunningApplicationInfo = new RunningApplicationInfo(packageName, isForeground, pid, context);
                    }

                    if (backgroundApplications.containsKey(packageName)) {
                        if (isForeground) {
                            // this app has background and foreground processes. Let us upgrade it to the foreground list
                            foregroundApplications.put(packageName, backgroundApplications.remove(packageName));
                            // note that the PID that is stored is the PID that showed in the first process,
                            // which is also the most important process since they are ordered
                        }
                    } else {
                        if (foregroundApplications.containsKey(packageName)) {
                            continue;  // already stored as foregroundApplication, not need to add again
                        } else {
                            // not yet stored
                            if (isForeground) {
                                foregroundApplications.put(packageName, new RunningApplicationInfo(packageName, isForeground, pid, context));
                            } else {
                                backgroundApplications.put(packageName, new RunningApplicationInfo(packageName, isForeground, pid, context));
                            }
                        }
                    }
                    // MyLogHelper.logDebug(TAG, "Collected running app " + packageName + " with pid " + pid + " is in a foreground state? " + isForeground);
                }
                contextData.foregroundRunningApplicationInfoArray = foregroundApplications.values().toArray(new RunningApplicationInfo[0]);
                contextData.backgroundRunningApplicationInfoArray = backgroundApplications.values().toArray(new RunningApplicationInfo[0]);
            }
        }

        private static boolean isCached(String oomAdjField) {
            return oomAdjField.contains("cch");
        }

        private static boolean isForeground(String isForegroundField) {
            return isForegroundField.equals("A") || isForegroundField.equals("S");
        }

        private static boolean hasUsageAccessPermission(Context context) {
            AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            if (appOps != null) {
                int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, Process.myUid(), context.getPackageName());
                return mode == AppOpsManager.MODE_ALLOWED;
            }
            return false;
        }

        private static String[] getCmdOutput() {
            try {
                java.lang.Process process = Runtime.getRuntime().exec(CMD_COMMAND);

                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                int read;
                char[] buffer = new char[4096];
                StringBuilder output = new StringBuilder();
                while ((read = reader.read(buffer)) > 0) {
                    output.append(buffer, 0, read);
                }
                reader.close();

                // Waits for the command to finish.
                process.waitFor();

                // MyLogHelper.logDebug(TAG, "output size = " + output.length() + " output: " + output.toString());
                return output.toString().split("\n");
            } catch (IOException | InterruptedException e) {
                MyLogHelper.logError(TAG, "Failed to read running processes. Returning null. " + e);
                return null;
            }
        }
    }
}
