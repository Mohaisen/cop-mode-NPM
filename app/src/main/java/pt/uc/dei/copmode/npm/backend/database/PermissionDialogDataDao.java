package pt.uc.dei.copmode.npm.backend.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAndContextData;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;

@Dao
public interface PermissionDialogDataDao {
    String PERMISSION_DIALOG_DATA_TABLE = "permission_dialog_data_table";

    @Insert(onConflict = OnConflictStrategy.ABORT)
    long insertPermissionDialogData(PermissionDialogData data);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    long[] insertPermissionDialogDataList(List<PermissionDialogData> data);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " ORDER BY timestamp DESC")
    LiveData<List<PermissionDialogData>> getAllPermissionDialogData();

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE answerType == :answerType ORDER BY timestamp ASC LIMIT 1")
    PermissionDialogData getFirstPermissionDialogDataWithAnswerType(PermissionDialogAnswerType answerType);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE answerType == :answerType AND timestamp >= :minTimestamp ORDER BY timestamp DESC")
    PermissionDialogData[] getAllPermissionDialogDataWithAnswerTypeSince(PermissionDialogAnswerType answerType, long minTimestamp);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE answerType != :answerType ORDER BY timestamp DESC")
    LiveData<List<PermissionDialogData>> getAllPermissionDialogDataExceptAnswerType(PermissionDialogAnswerType answerType);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE requesting_packageName = :packageName AND timestamp >= :minTimestamp AND answerType = :answerType ORDER BY timestamp DESC")
    PermissionDialogData[] getOrderedPckgPermissionDialogData(String packageName, long minTimestamp, PermissionDialogAnswerType answerType);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE requesting_packageName = :packageName AND checkedPermission = :checkedPermission AND timestamp >= :minTimestamp AND answerType = :answerType ORDER BY timestamp DESC" )
    PermissionDialogData[] getOrderedFilteredPermissionDialogDataByPermission(String packageName, String checkedPermission, long minTimestamp, PermissionDialogAnswerType answerType);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE requesting_packageName = :packageName AND checkedPermissionGroup = :checkedPermissionGroup AND timestamp >= :minTimestamp AND answerType = :answerType ORDER BY timestamp DESC" )
    PermissionDialogData[] getOrderedFilteredPermissionDialogDataByPermissionGroup(String packageName, String checkedPermissionGroup, long minTimestamp, PermissionDialogAnswerType answerType);

    @Transaction
    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " ORDER BY timestamp DESC")
    List<PermissionDialogAndContextData> getAllPermissionDialogAndContextData();

    @Transaction
    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE timestamp >= :timestamp ORDER BY timestamp DESC")
    LiveData<List<PermissionDialogAndContextData>> getPermissionDialogAndContextDataSinceTimestamp(long timestamp);

    @Query("SELECT count(permissionDialogDataId) FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE answerType == :answerType AND timestamp >= :minTimestamp")
    int countPermissionDialogDataWithAnswerTypeSince(PermissionDialogAnswerType answerType, long minTimestamp);

    @Query("SELECT * FROM " + PERMISSION_DIALOG_DATA_TABLE + " WHERE requesting_packageName = :packageName AND checkedPermissionGroup = :checkedPermissionGroup AND timestamp >= :minTimestamp AND answerType = :answerType LIMIT 1" )
    PermissionDialogData getSingleFilteredPermissionForPermissionGroup(String packageName, String checkedPermissionGroup, long minTimestamp, PermissionDialogAnswerType answerType);
}
