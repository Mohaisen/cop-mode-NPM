package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class ScreenModeReceiver extends BroadcastReceiver implements ContextReceiver {
    private final static String TAG = "ScreenModeReceiver";
    private boolean isReceiving = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return;

        if (action.equals(Intent.ACTION_SCREEN_OFF)) {
            MyLogHelper.logDebug(TAG, "Screen switched to off");
            ContextualDataHolder.getInstance(context).updateScreenIsInteractive(false);
            ContextualDataHolder.getInstance(context).updateIsKeyguardLocked(true);
        } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
            MyLogHelper.logDebug(TAG, "Screen is now interactive");
            ContextualDataHolder.getInstance(context).updateScreenIsInteractive(true);
        }
    }

    @Override
    public boolean registerSelf(Context context) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        context.registerReceiver(this, filter);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }
}
