package pt.uc.dei.copmode.npm.xposed;

import android.util.Log;

import java.lang.reflect.Method;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

import static de.robv.android.xposed.XposedHelpers.findMethodExactIfExists;

public class XHook {
    private final String TAG = "xposed/XHook";

    private String className;
    private String methodName;
    private Object[] parameterTypes;
    private XC_MethodHook hookHandler;

    XHook(String className, String methodName, XC_MethodHook hookHandler, Object... parameterTypes) {
        this.className = className;
        this.methodName = methodName;
        this.hookHandler = hookHandler;
        this.parameterTypes = parameterTypes;
    }

    void hook(XC_LoadPackage.LoadPackageParam lpparam) {
        Class<?> clazz = XposedHelpers.findClassIfExists(className, lpparam.classLoader);
        if (clazz != null) {
            Method method = findMethodExactIfExists(clazz, methodName, parameterTypes);
            if (method != null) {
                XposedBridge.hookMethod(method, hookHandler);
            } else {
                XposedLogger.logWarn(null, TAG, "Couldn't hook method " + methodName + " for class " + className + ". Method not found.");
            }
        } else {
            XposedLogger.logWarn(null, TAG, "Couldn't hook class " + className + ". Class not found.");
        }
    }
}
