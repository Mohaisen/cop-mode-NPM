package pt.uc.dei.copmode.npm.receivers;

import android.content.Context;

public interface ContextReceiver {
    // returns true if successfully registered and false otherwise
    boolean registerSelf(Context context);
    boolean isReceiving();
}
