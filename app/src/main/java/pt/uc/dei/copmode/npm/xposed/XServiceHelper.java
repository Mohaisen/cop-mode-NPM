package pt.uc.dei.copmode.npm.xposed;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import pt.uc.dei.copmode.npm.BuildConfig;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

/**
 * This helper class contains methods to allow to communication with the Permission Manager Service.
 *
 * The XApplicationOnCreateHookHandle calls bindPermissionManagerService() just after the Application
 * instance is created, such that all communications for each app go through a single ServiceConnection.
 */
public class XServiceHelper {
    private final static String TAG = "xposed/XServiceHelper";

    private static final Intent bindServiceIntent = new Intent().setComponent(new ComponentName(BuildConfig.APPLICATION_ID, BuildConfig.APPLICATION_ID + ".services.PermissionManagerService"));

    private static String packageName;

    public static boolean isBinding = false;
    public static boolean bound = false;

    public static Messenger npmServiceOutMessenger;  // sends message to service
    private static CountDownLatch bindingCountDownLatch = new CountDownLatch(1);

    /**
     * This map contains the countDownLatchs associated with the threads waiting for a service response (e.g. the dialog results).
     */
    public static final Map<Integer, CountDownLatch> threadIdToDialogCountDownLatchMap = new HashMap<>();

    /**
     * This list has all the unhandled PermissionDialogData.
     * Because the service takes some time to be bound and we can't lock the main thread, some permission checks will be missed.
     * We store such permissions in this list, and when the service connects, we communicate them.
     */
    public static List<PermissionDialogData> unhandledPermissionDialogDataList;

    public static ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            XposedLogger.logDebug(TAG, packageName, "Permission Manager Service bound");
            npmServiceOutMessenger = new Messenger(service);
            bound = true;
            isBinding = false;
            bindingCountDownLatch.countDown();

            if (unhandledPermissionDialogDataList != null && !unhandledPermissionDialogDataList.isEmpty()) {
                XposedLogger.logDebug(TAG, packageName, "Found unhandled permission checks after connecting to service. Reporting to service.");
                // handle this in separate thread for performance
                new Thread() {
                    @Override
                    public void run() {
                        // we have to turn the data into a single bundle before sending
                        ArrayList<Bundle> permissionDialogDataBundleList = new ArrayList<>(unhandledPermissionDialogDataList.size());
                        for (PermissionDialogData unhandled: unhandledPermissionDialogDataList) {
                            permissionDialogDataBundleList.add(unhandled.toBundle());
                        }
                        Bundle dataToService = new Bundle();
                        dataToService.putParcelableArrayList(PermissionManagerService.KEY_UNHANDLED_PACKAGES, permissionDialogDataBundleList);

                        Message msg = Message.obtain(null, PermissionManagerService.MSG_UNHANDLED_PERMISSION_CHECKS, dataToService);
                        Exception error = XServiceHelper.trySendingMessage(msg);
                        if (error != null) {
                            XposedLogger.logDebug(TAG, packageName, "Error sending unhandled permission checks: " + error.toString());
                        } else {
                            unhandledPermissionDialogDataList.clear();
                        }
                    }
                }.start();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            XposedLogger.logError(TAG, packageName, "Permission Manager Service Disconnected (crashed/killed)");
            npmServiceOutMessenger = null;
            bound = false;
            isBinding = false;
            bindingCountDownLatch.countDown();

            // unlock all waiting threads
            synchronized (this) {
                if (!threadIdToDialogCountDownLatchMap.isEmpty()) {
                    XposedLogger.logDebug(TAG, packageName, "Cleaning threadIdToDialogCountDownLatchMap");
                    Iterator<Map.Entry<Integer, CountDownLatch>> it = threadIdToDialogCountDownLatchMap.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry<Integer, CountDownLatch> entry = it.next();
                        entry.getValue().countDown();
                        it.remove();
                    }
                }
            }

            // the service will attempt to reconnect, so there's no need to do anything
        }
    };

    public static boolean bindPermissionManagerService(Context applicationContext) {
        packageName = applicationContext.getPackageName();
        if(!bound || !isBinding) {
            isBinding = applicationContext.bindService(bindServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
        return isBinding;
    }

    public static boolean awaitBind(@Nullable Long timeoutSeconds) throws InterruptedException {
        if (timeoutSeconds == null) {
            bindingCountDownLatch.await();
            return true;
        } else {
            return bindingCountDownLatch.await(timeoutSeconds, TimeUnit.SECONDS);
        }
    }

    public static Exception trySendingMessage(Message msg) {
        if (!bound)
            return new Exception("Attempted to send a message but service is not yet bound.");
        try {
            npmServiceOutMessenger.send(msg);
        } catch (RemoteException e) {
            // If we reach here, means that the connection to the service is broken, so we force disconnect
            serviceConnection.onServiceDisconnected(null);
            return e;
        }
        return null;
    }

    public synchronized static CountDownLatch addThreadWaitingServiceResponse(int threadId, CountDownLatch dialogCountDownLatch) {
        return threadIdToDialogCountDownLatchMap.put(threadId, dialogCountDownLatch);
    }

    public synchronized static CountDownLatch rmThreadWaitingServiceResponse(int threadId) {
        return threadIdToDialogCountDownLatchMap.remove(threadId);
    }

    public synchronized static CountDownLatch getLatchFromWaitingThreadId(int threadId) {
        return threadIdToDialogCountDownLatchMap.get(threadId);
    }

    public static void addUnhandledPermission(PermissionDialogData permissionDialogData) {
        if(unhandledPermissionDialogDataList == null)
            unhandledPermissionDialogDataList = new ArrayList<>();  // lazy loading
        unhandledPermissionDialogDataList.add(permissionDialogData);
    }
}
