package pt.uc.dei.copmode.npm.xposed;

import android.app.AndroidAppHelper;
import android.content.Context;

import androidx.annotation.Nullable;

import java.lang.reflect.Method;
import java.util.Arrays;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;

import static de.robv.android.xposed.XposedBridge.unhookMethod;

public class XOnRequestPermissionsResultHookHandler  extends XC_MethodHook {
    private final static String TAG = "xposed/XOnRequestPermissionsResultHookHandler";

    private Context applicationContext;
    private String packageName;

    @Override
    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
        applicationContext = AndroidAppHelper.currentApplication().getApplicationContext();
        packageName = applicationContext.getPackageName();
        // parameters
        int requestCode = (int) param.args[0];
        String[] requestingPermissions = (String[]) param.args[1];
        int[] grantResults = (int[]) param.args[2];

        XposedLogger.logDebug(TAG, packageName, "In beforeHook for onRequestPermissionsResult with original grantResults = " + Arrays.toString(grantResults));

        // since we calling this method with different parameters, we first unhook the method to
        // avoid cyclic handle (the hook is added every time requestPermissions is called)
        unhookMethod(param.method, this);

        grantResults = fetchGrantResults(requestingPermissions);  // override the parameter
        if (grantResults == null) {  // we failed to get results, so we let the normal flow handle
            return;
        }

        param.setResult(null);  // we set the result to abort further execution of the original call
        Object[] newArgs = {requestCode, requestingPermissions, grantResults};
        XposedLogger.logDebug(TAG, packageName, "Calling onRequestPermissionsResult with changed grantResults. New values: " + Arrays.toString(grantResults));
        XposedBridge.invokeOriginalMethod(param.method, param.thisObject, newArgs);
    }

    @Nullable
    private int[] fetchGrantResults(String[] requestingPermissions) {
        // because an app verifies if it has the permission before requesting, the grant results for all requested permissions should be in the cache.
        // so we attempt to fetch from the cache, if it fails, we force a permission check that will trigger our check permission hook handler.
        int[] grantResults = new int[requestingPermissions.length];
        XposedLogger.logDebug(TAG, packageName, "Fetching grant results. requestingPermissions=" + Arrays.toString(requestingPermissions));
        for (int i=0; i<requestingPermissions.length; i++) {
            String permission = requestingPermissions[i];
            String permissionGroup = XposedHelper.getPermissionGroup(applicationContext, permission);
            XCachedPermissionResult cachedResult = XCheckPermissionHookHandler.getCachedResult(permission, permissionGroup);
            if (cachedResult != null && cachedResult.stillValid()) {
                XposedLogger.logDebug(TAG, packageName, "Permission in cache: " + cachedResult);
                grantResults[i] = cachedResult.grantResult;
            } else {
                // if the permission is not in cache it was probably not checked yet. This should be very rare given that apps should check before requesting
                XposedLogger.logDebug(TAG, packageName, "Permission " + permission + " is not in cache. Force checking permission.");

                if (cachedResult != null)  // remove invalid result from cache if there's any
                    XCheckPermissionHookHandler.removeCachedResult(permission, permissionGroup);

                applicationContext.checkSelfPermission(permission);  // we force the XCheckPermissionHook, which will populate the cache
                cachedResult = XCheckPermissionHookHandler.getCachedResult(permission, permissionGroup);
                if (cachedResult != null && cachedResult.stillValid()) {
                    XposedLogger.logDebug(TAG, packageName, "Got permission result after forcing permission check: " + cachedResult);
                    grantResults[i] = cachedResult.grantResult;
                } else {
                    // this should never happen
                    XposedLogger.logError(TAG, packageName, "Failed to retrieve permission result after forcing permission check.");
                    return null;
                }
            }
        }
        return grantResults;
    }
}
