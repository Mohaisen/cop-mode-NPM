package pt.uc.dei.copmode.npm.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.provider.CallLog;
import android.util.Log;

import java.util.Date;

import pt.uc.dei.copmode.npm.ICheckCallingPermissionTesterService;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class CheckCallingPermissionTesterService extends Service {
    private static final String TAG = "CheckCallingPermissionTesterService";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        return binder;
    }

    // To test this, change in XPermissions handleLoadPackage() to allow to hook CM-NPM
    private final ICheckCallingPermissionTesterService.Stub binder = new ICheckCallingPermissionTesterService.Stub() {

        @Override
        public void test() {
            MyLogHelper.logDebug(TAG, "In test calling permission");
            MyLogHelper.logDebug(TAG, "callUid="+ Binder.getCallingUid()+" myUid="+ Process.myUid() + " calling pid =" + Binder.getCallingPid() + " my pid = " + Process.myPid());

            MyLogHelper.logDebug(TAG, "Self has location permission? " + (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED));
            MyLogHelper.logDebug(TAG, "Calling has location permission? " + (checkCallingPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED));

            MyLogHelper.logDebug(TAG, "Self has read calendar permission? " + (checkSelfPermission(Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED));
            MyLogHelper.logDebug(TAG, "Calling has read calendar permission? " + (checkCallingPermission(Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED));

            MyLogHelper.logDebug(TAG, "Self has call log permission? " + (checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED));
            MyLogHelper.logDebug(TAG, "Calling has call log permission? " + (checkCallingPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED));

            MyLogHelper.logDebug(TAG, "Now we test the call log permission. This only works if NPM has the permission, else the calling app throws an exception.");
            readCallLog();
            /*
            Add to manifest and allow the permission if you don't want the other app to crash:
                <uses-permission android:name="android.permission.READ_CALL_LOG" />
                <uses-permission android:name="android.permission.WRITE_CALL_LOG" />
             */
        }
    };

    private void readCallLog() {
        long dialed;
        String number;
        String columns[]=new String[] {
                CallLog.Calls._ID,
                CallLog.Calls.NUMBER,
                CallLog.Calls.DATE,
                CallLog.Calls.DURATION,
                CallLog.Calls.TYPE};
        Cursor c;
        c = getContentResolver().query(Uri.parse("content://call_log/calls"),
                columns, null, null, "Calls._ID DESC"); //last record first
        while (c.moveToNext()) {
            dialed=c.getLong(c.getColumnIndex(CallLog.Calls.DATE));
            number=c.getString(c.getColumnIndex(CallLog.Calls.NUMBER));
            MyLogHelper.logInfo("CallLog","type: " + c.getString(4) + "Call to number: "+number+", registered at: "+new Date(dialed).toString());
        }
    }
}
