package pt.uc.dei.copmode.npm.frontend.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.frontend.activities.MainActivity;

public class MainActivityViewModel extends ViewModel {
    public final LiveData<List<PermissionDialogData>> permissionDialogDataList;

    public MainActivityViewModel(PermissionDialogDataDao dao) {
        this.permissionDialogDataList = dao.getAllPermissionDialogDataExceptAnswerType(PermissionDialogAnswerType.UNHANDLED);
    }

    public static MainActivityViewModel fromProvider(ViewModelStoreOwner viewModelStoreOwner, final PermissionDialogDataDao permissionDialogDataDao) {
        return new ViewModelProvider(viewModelStoreOwner, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                try {
                    return modelClass.getConstructor(PermissionDialogDataDao.class).newInstance(permissionDialogDataDao);
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }).get(MainActivityViewModel.class);
    }
}
