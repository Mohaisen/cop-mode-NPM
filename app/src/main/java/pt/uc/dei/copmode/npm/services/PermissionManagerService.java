package pt.uc.dei.copmode.npm.services;

import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Process;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.frontend.activities.PermissionDialogActivity;
import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.ContextData;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.frontend.fragments.PermissionDialogFragment;
import pt.uc.dei.copmode.npm.helpers.JobSchedulerHelper;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.helpers.NotificationHelper;
import pt.uc.dei.copmode.npm.receivers.BatteryReceiver;
import pt.uc.dei.copmode.npm.receivers.BluetoothReceiver;
import pt.uc.dei.copmode.npm.receivers.CalendarReceiver;
import pt.uc.dei.copmode.npm.receivers.CallReceiver;
import pt.uc.dei.copmode.npm.receivers.ConnectivityReceiver;
import pt.uc.dei.copmode.npm.receivers.ContextReceiver;
import pt.uc.dei.copmode.npm.receivers.DockReceiver;
import pt.uc.dei.copmode.npm.receivers.KeyguardReceiver;
import pt.uc.dei.copmode.npm.receivers.LocationReceiver;
import pt.uc.dei.copmode.npm.receivers.ScreenModeReceiver;
import pt.uc.dei.copmode.npm.receivers.WifiReceiver;


public class PermissionManagerService extends Service {
    private static final String TAG = "PermissionManagerService";

    private static final String CHANNEL_ID = "pt.uc.dei.copmode.npm.permissionmanagerservice.notification.CHANNEL_ID_FOREGROUND";
    private static final int NOTIFICATION_ID = 1;

    public static final String ACTION_RESTART_SERVICES = "pt.uc.dei.copmode.npm.permissionmanagerservice.action.RESTART_SERVICES";

    public static final String KEY_PCKG_NAME = "pt.uc.dei.copmode.npm.permissionmanagerservice.key.PCKG_NAME";
    public static final String KEY_PCKG_VALID_PERMISSIONS = "pt.uc.dei.copmode.npm.permissionmanagerservice.key.PCKG_VALID_PERMISSIONS";
    public static final String KEY_UNHANDLED_PACKAGES = "pt.uc.dei.copmode.npm.permissionmanagerservice.key.UNHANDLED_PACKAGES";

    public static final int MSG_NEW_PROMPT_CLIENT = 0;
    public static final int MSG_PROMPT_COMPLETE = 1;
    public static final int MSG_CACHED_RESPONSE = 2;
    public static final int MSG_PROMPT_DISMISSED = 3;
    public static final int MSG_GET_PCKG_VALID_PERMISSIONS = 4;
    public static final int MSG_UNHANDLED_PERMISSION_CHECKS = 5;

    private Messenger mMessenger;
    private HandlerThread messengerHandlerThread;  // separate thread to handle dialog request comns

    private List<ContextReceiver> registeredReceivers;

    class IncomingPromptMessageHandler extends Handler {
        private final Context applicationContext;
        private final PermissionDialogDataDao permissionDialogDataDao;

        private final Queue<PromptClient> promptClientsQueue = new LinkedList<>();
        private PromptClient processingPromptClient;

        // If for some reason there is no response from PermissionDialogActivity, we have to timeout and release the processingPromptClient
        private final Handler dialogTimeoutHandler;
        private final Runnable dialogTimeoutRunnable = new Runnable() {
            @Override
            public void run() { // this only runs if PermissionDialogActivity fails to complete in time (due to any possible error)
                MyLogHelper.logWarn(TAG, "Timedout while Waiting for a response from PermissionDialogActivity. Sending timeout response to app.");
                // In this case we send a timedout response
                processingPromptClient.permissionDialogData.answerType = PermissionDialogAnswerType.TIMEDOUT;

                String error = sendPromptResults(processingPromptClient.permissionDialogData, MSG_PROMPT_COMPLETE, applicationContext);
                if (error != null) {
                    MyLogHelper.logError(TAG, error);
                }
                processingPromptClientComplete(true);  // frees the queue
            }
        };
        

        IncomingPromptMessageHandler(Looper looper, Context context) {
            super(looper);
            applicationContext = context.getApplicationContext();
            permissionDialogDataDao = NPMDatabase.getDB(applicationContext).permissionDialogDataDao();
            dialogTimeoutHandler = new Handler(looper);
        }

        @Override
        public void handleMessage(@NonNull final Message incomingMessage) {
            // data from/to
            // MyLogHelper.logDebug(TAG, "Incoming message: " + incomingMessage);
            // todo I should be using Message.setData() and Message.getData()
            switch (incomingMessage.what) {
                case MSG_NEW_PROMPT_CLIENT:
                    handleNewPromptClient(incomingMessage);
                    break;
                case MSG_PROMPT_COMPLETE:
                    handlePromptComplete(incomingMessage);
                    break;
                case MSG_CACHED_RESPONSE:
                    handleClientCachedResponse(incomingMessage);
                    break;
                case MSG_PROMPT_DISMISSED:
                    handlePromptDismissed(incomingMessage);
                    break;
                case MSG_GET_PCKG_VALID_PERMISSIONS:
                    handleGetPckgValidPermissions(incomingMessage);
                    break;
                case MSG_UNHANDLED_PERMISSION_CHECKS:
                    handleUnhandledPermissionChecks(incomingMessage);
                    break;
                default:
                    IncomingPromptMessageHandler.super.handleMessage(incomingMessage);
            }
        }

        void handleNewPromptClient(Message msg) {
            if(ContextualDataHolder.getInstance(this.applicationContext).shouldPrompt()) {
                MyLogHelper.logDebug(TAG, "Received new prompt client and phone is interactive. Adding to prompt queue.");
                PermissionDialogData permissionDialogData = PermissionDialogData.fromBundle((Bundle) msg.obj);
                PromptClient newPromptClient = new PromptClient(permissionDialogData, msg.replyTo);
                newPromptClient.collectContextData(applicationContext);
                promptClientsQueue.add(newPromptClient);
                tryPromptingNextDialog();
            } else {
                // Phone is not interactive/unlocked, so we shouldn't prompt the permission
                MyLogHelper.logDebug(TAG, "Received new prompt client but is not interactive. Sending timedout to client.");
                Bundle bundledPermissionDialogData = (Bundle) msg.obj;
                PermissionDialogData permissionDialogData = PermissionDialogData.fromBundle(bundledPermissionDialogData);
                permissionDialogData.answerType = PermissionDialogAnswerType.TIMEDOUT;  // set as timedout
                try {
                    msg.replyTo.send(Message.obtain(null, MSG_PROMPT_COMPLETE, permissionDialogData.toBundle()));
                } catch (RemoteException | NullPointerException e) {
                    MyLogHelper.logError(TAG, "Failed to forward timedout (phone not interactive) to client: " + e.getMessage());
                }
            }
        }

        private synchronized void tryPromptingNextDialog() {
            if(processingPromptClient == null && !promptClientsQueue.isEmpty()) {
                processingPromptClient = promptClientsQueue.remove();
                MyLogHelper.logDebug(TAG, "Starting to process new prompt client for app " +
                        processingPromptClient.permissionDialogData.requestingApplicationInfo.packageName +
                        " and permission " + processingPromptClient.permissionDialogData.checkedPermissionGroup);
                PermissionDialogData permissionDialogData = processingPromptClient.permissionDialogData;

                // Check if this permission request was answered recently.
                PermissionDialogData validCachedAnswer = permissionDialogDataDao.getSingleFilteredPermissionForPermissionGroup(permissionDialogData.requestingApplicationInfo.packageName,
                        permissionDialogData.checkedPermissionGroup,
                        PermissionDialogData.getNonExpiredStartTimestamp(),
                        PermissionDialogAnswerType.USER_ANSWERED);

                if (validCachedAnswer != null) {
                    MyLogHelper.logDebug(TAG, "Found non-expired permission response in cache. Returning it to client");
                    PermissionDialogData response = validCachedAnswer.createCacheCopy(permissionDialogData.method, permissionDialogData.threadId);  // the first is the most recent
                    Message returnMsg = Message.obtain(null, MSG_PROMPT_COMPLETE, response.toBundle());
                    try {
                        processingPromptClient.replyToMessenger.send(returnMsg);
                    } catch (RemoteException e) {
                        MyLogHelper.logDebug(TAG, "Failed to forward cached response to prompt issuer.");
                        processingPromptClientComplete(false);
                        return;
                        // this.removeMessages(returnMsg.what, returnMsg.obj);
                    }
                    permissionDialogDataDao.insertPermissionDialogData(response);
                    processingPromptClientComplete(false);
                    return;
                }
                MyLogHelper.logDebug(TAG, "No cache response was found. Prompting permission request.");

                // We should probably only prompt if screen is interactive. But this way we might get more data
                showPermissionPromptAsActivity(permissionDialogData);

                // we start the timeout timer and we wait 1.5*TIMEOUT_SECONDS.
                dialogTimeoutHandler.postDelayed(dialogTimeoutRunnable, Math.round(1.5*PermissionDialogFragment.TIMEOUT_SECONDS*1000));
            }
        }

        private synchronized void processingPromptClientComplete(boolean saveToDb) {
            if (saveToDb)
                processingPromptClient.saveToDb(applicationContext);
            processingPromptClient = null;
            tryPromptingNextDialog();
        }

        void handlePromptComplete(Message msg) {
            MyLogHelper.logDebug(TAG, "Prompt complete. Returning results to caller and saving in DB");

            // remove the timeout runnable
            dialogTimeoutHandler.removeCallbacks(dialogTimeoutRunnable);

            Bundle bundledPermissionDialogData = (Bundle) msg.obj;
            PermissionDialogData permissionDialogData = PermissionDialogData.fromBundle(bundledPermissionDialogData);

            // We check for null because if the client timedout through dialogTimeoutHandler it can now be null -- this should never happen because we give enough time
            if (processingPromptClient == null || processingPromptClient.permissionDialogData.getPermissionDialogDataId() != permissionDialogData.getPermissionDialogDataId()) {
                MyLogHelper.logError(TAG, "Prompt completed for different PermissionDialogData! Inconsistent state...");
                return;
            }
            processingPromptClient.permissionDialogData = permissionDialogData;

            String error = sendPromptResults(permissionDialogData, MSG_PROMPT_COMPLETE, applicationContext);
            if (error != null) {
                MyLogHelper.logError(TAG, error);
            }
            processingPromptClientComplete(true);
        }

        void handleClientCachedResponse(Message msg) {
            MyLogHelper.logDebug(TAG, "Received cached response. Storing in DB.");
            PermissionDialogData permissionDialogData = PermissionDialogData.fromBundle((Bundle) msg.obj);
            permissionDialogData.answerType = PermissionDialogAnswerType.CACHE_ANSWERED;  // overwrite to be safe
            permissionDialogDataDao.insertPermissionDialogData(permissionDialogData);
        }

        void handlePromptDismissed(Message msg) {
            MyLogHelper.logDebug(TAG, "Received prompt dismissed. Sending dismissed prompt to client and storing in DB.");

            // remove the timeout runnable
            dialogTimeoutHandler.removeCallbacks(dialogTimeoutRunnable);

            PermissionDialogData permissionDialogData = PermissionDialogData.fromBundle((Bundle) msg.obj);
            // We check for null because if the client timedout through dialogTimeoutHandler it can now be null -- this should never happen because we give enough time
            if (processingPromptClient == null || processingPromptClient.permissionDialogData.getPermissionDialogDataId() != permissionDialogData.getPermissionDialogDataId()) {
                MyLogHelper.logError(TAG, "Prompt completed for different PermissionDialogData! Inconsistent state...");
                return;
            }
            permissionDialogData.answerType = PermissionDialogAnswerType.DISMISSED;  // overwrite to be safe
            processingPromptClient.permissionDialogData = permissionDialogData;
            String error = sendPromptResults(permissionDialogData, MSG_PROMPT_DISMISSED, applicationContext);
            if (error != null) {
                MyLogHelper.logError(TAG, error);
            }
            permissionDialogDataDao.insertPermissionDialogData(permissionDialogData);
            processingPromptClientComplete(false);
        }

        private void handleGetPckgValidPermissions(Message msg) {
            MyLogHelper.logDebug(TAG, "Received get package valid permissions. Sending to client");
            Bundle received = (Bundle) msg.obj;
            String pckgName = (received != null) ? received.getString(KEY_PCKG_NAME) : null;
            if (pckgName == null) {
                MyLogHelper.logError(TAG, "Received a get package valid request with empty package name. Bug? Ignoring");
                return;
            }
            PermissionDialogData[] pckgPermissions = permissionDialogDataDao.getOrderedPckgPermissionDialogData(
                    pckgName, PermissionDialogData.getNonExpiredStartTimestamp(), PermissionDialogAnswerType.USER_ANSWERED
            );
            // this sucks, but what's the alternative?
            ArrayList<Bundle> permissionDataBundleArray = new ArrayList<>(pckgPermissions.length);
            for (PermissionDialogData permissionData: pckgPermissions) {
                permissionDataBundleArray.add(permissionData.toBundle());
            }
            Bundle toReturnBundle = new Bundle();
            toReturnBundle.putParcelableArrayList(KEY_PCKG_VALID_PERMISSIONS, permissionDataBundleArray);
            Message returnMsg = Message.obtain(null, MSG_GET_PCKG_VALID_PERMISSIONS, toReturnBundle);
            try {
                msg.replyTo.send(returnMsg);
            } catch (RemoteException e) {
                MyLogHelper.logDebug(TAG, "Failed to send pckg valid permissions.");
                return;
                // this.removeMessages(returnMsg.what, returnMsg.obj);
            }
        }

        private void handleUnhandledPermissionChecks(Message msg) {
            MyLogHelper.logDebug(TAG, "Received unhandled permission checks. Storing in DB");
            // we do this in a different thread for performance.
            final Bundle bundledPermissionDialogDataList = (Bundle) msg.obj;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<Bundle> listOfBundledData = bundledPermissionDialogDataList.getParcelableArrayList(KEY_UNHANDLED_PACKAGES);
                    List<PermissionDialogData> dataList = new ArrayList<>();
                    for (Bundle bundledData: listOfBundledData) {
                        dataList.add(PermissionDialogData.fromBundle(bundledData));
                    }
                    // MyLogHelper.logDebug(TAG, "Storing the following unhandledPermissionChecks in DB: " + dataList);
                    permissionDialogDataDao.insertPermissionDialogDataList(dataList);
                }
            }).start();
        }

        private String sendPromptResults(PermissionDialogData permissionDialogData, int msgType, Context context) {
            try {
                // forward results to prompt issuer
                processingPromptClient.replyToMessenger.send(Message.obtain(null, msgType, permissionDialogData.toBundle()));
            } catch (RemoteException | NullPointerException e) {
                return "Failed to forward prompt results to issuer client.";
            }
            return null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT).show();
        return mMessenger.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MyLogHelper.logDebug(TAG, "Starting " + TAG);
        if (intent != null) {
            String action = intent.getAction();
            if (action!= null && action.equals(ACTION_RESTART_SERVICES)) {
                MyLogHelper.logDebug(TAG, "Got restart services action. Restarting services");
                tryStartStoppedReceivers(getApplicationContext());
            }
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createAndShowForegroundNotification();

        Context applicationContext = getApplicationContext();

        messengerHandlerThread = new HandlerThread("NPMServiceMessengerHandlerThread");
        messengerHandlerThread.start();
        Looper looper = messengerHandlerThread.getLooper();
        mMessenger = new Messenger(new IncomingPromptMessageHandler(looper, applicationContext));

        startReceivers(applicationContext);

        JobSchedulerHelper.startPeriodicJobs(applicationContext);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        messengerHandlerThread.quitSafely();
    }

    void startReceivers(Context context) {
        // todo for performance, I should set a new thread for the receivers.
        ContextualDataHolder.getInstance(context); // instantiate the contextual data holder with the service as context.

        // register receivers
        registeredReceivers = new ArrayList<>(9);
        registeredReceivers.add(new ConnectivityReceiver());
        registeredReceivers.add(new ScreenModeReceiver());
        registeredReceivers.add(new KeyguardReceiver());
        registeredReceivers.add(new DockReceiver());
        registeredReceivers.add(new CallReceiver());
        registeredReceivers.add(new BatteryReceiver());
        registeredReceivers.add(new LocationReceiver());
        registeredReceivers.add(new WifiReceiver());
        registeredReceivers.add(new BluetoothReceiver());
        registeredReceivers.add(new CalendarReceiver());

        for (ContextReceiver receiver: registeredReceivers) {
            boolean success = receiver.registerSelf(context);
            if (!success) {
                MyLogHelper.logDebug(TAG, "Failed to register receiver: " + receiver.getClass().getName());
            }
        }
    }

    private void tryStartStoppedReceivers(Context context) {
        for (ContextReceiver receiver: registeredReceivers) {
            if (!receiver.isReceiving()) {
                boolean success = receiver.registerSelf(context);
                if (!success) {
                    MyLogHelper.logDebug(TAG, "Failed to re-start receiver: " + receiver.getClass().getName());
                }
            }
        }
    }

    private void showPermissionPromptAsActivity(PermissionDialogData clientData) {
        Intent intent = new Intent(this, PermissionDialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PermissionDialogData.INTENT_KEY, clientData.toBundle());
        this.startActivity(intent);
    }

    private void createAndShowForegroundNotification() {
        Notification runningStatusNotification = NotificationHelper.createRunningStatusNotification(this);
        this.startForeground(NotificationHelper.RUNNING_STATUS_NOTIFICATION_ID, runningStatusNotification);
    }

    public static void startServiceIfOff(Context context) {
        boolean isServiceRunning = false;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                // MyLogHelper.logDebug(TAG, service.service.getClassName());
                if (PermissionManagerService.class.getName().equals(service.service.getClassName())) {
                    isServiceRunning = true;
                    break;
                }
            }
        } else {
            MyLogHelper.logError(TAG, "Null Activity Manager");
        }
        if (!isServiceRunning) {
            // Even if we get a null activity manager, we can try to start the service
            Intent serviceIntent = new Intent(context, PermissionManagerService.class);
            ContextCompat.startForegroundService(context, serviceIntent);
        }
    }

    public static boolean hasUsageAccessPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        if (appOps != null) {
            int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, Process.myUid(), context.getPackageName());
            return mode == AppOpsManager.MODE_ALLOWED;
        }
        return false;
    }

    @Deprecated
    private AlertDialog buildPermissionDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.AppTheme);

        dialogBuilder.setTitle("Title");
        dialogBuilder.setMessage("Message");
        dialogBuilder.setNegativeButton("Back",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );

        final AlertDialog dialog = dialogBuilder.create();
        final Window dialogWindow = dialog.getWindow();
        final WindowManager.LayoutParams dialogWindowAttributes = dialogWindow.getAttributes();

        // Set fixed width (280dp) and WRAP_CONTENT height
        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogWindowAttributes);
        lp.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 280, getResources().getDisplayMetrics());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogWindow.setAttributes(lp);

        return dialog;
    }

    @Deprecated
    private void showPermissionPromptAsSystemDialog() {
        // This solution works, but requires permission to draw over other apps. Additionally,
        // it won't work in Android 10 and onwards as it requires SYSTEM_ALERT_WINDOW:
        // https://developer.android.com/about/versions/10/behavior-changes-all#sysalert-go
        // So this is no longer used. We now show a transparent activity with the dialog.

        AlertDialog dialog = buildPermissionDialog();

        // Set to TYPE_SYSTEM_ALERT so that the Service can display it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }
        dialog.show();
    }

    private static class PromptClient {
        PermissionDialogData permissionDialogData;
        Messenger replyToMessenger;
        ContextData contextData;

        PromptClient(PermissionDialogData data, Messenger messenger) {
            permissionDialogData = data;
            replyToMessenger = messenger;
        }

        void collectContextData(Context context) {
            // MyLogHelper.logDebug(TAG, "Collecting contextual data");
            contextData = ContextualDataHolder.getInstance(context).getCurrentContextCopy(context);
        }

        void saveToDb(Context context) {
            // MyLogHelper.logDebug(TAG, "Storing permission dialog data and context in DB");
            contextData.permissionDialogDataOwnerId = NPMDatabase.getDB(context)
                    .permissionDialogDataDao()
                    .insertPermissionDialogData(permissionDialogData);
            NPMDatabase.getDB(context).contextDataDao().insertContextData(contextData);
        }
    }
}
