package pt.uc.dei.copmode.npm.receivers;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class CallReceiver implements ContextReceiver {
    private static final String TAG = "CallReceiver";
    private boolean isReceiving = false;

    @Override
    public boolean registerSelf(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new MyPhoneStateListener(context), PhoneStateListener.LISTEN_CALL_STATE);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }

    static class MyPhoneStateListener extends PhoneStateListener {
        private Context context;
        MyPhoneStateListener(Context context) {
            this.context = context;
        }

        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
            MyLogHelper.logDebug(TAG, "Call state changed. New state: " + state);
            ContextualDataHolder.getInstance(context).updateCallState(state);
        }
    }
}
