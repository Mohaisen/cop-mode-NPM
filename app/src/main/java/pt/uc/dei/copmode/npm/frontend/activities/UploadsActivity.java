package pt.uc.dei.copmode.npm.frontend.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.work.Data;
import androidx.work.WorkInfo;

import com.google.android.material.snackbar.Snackbar;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.DateHelper;
import pt.uc.dei.copmode.npm.components.Uploader;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.helpers.PhoneIdHelper;

public class UploadsActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "UploadsActivity";

    private TextView dataToSendTV;
    private TextView lastUploadDateTV;
    private ProgressBar progressBar;
    private TextView uploadErrorTextView;
    private Button uploadButton;

    private long lastUploadTimestamp;

    private Uploader uploaderInstance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploads);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.uploads_actionbar_title);
        }

        TextView phoneIdTV = findViewById(R.id.phone_id_textview);
        phoneIdTV.setText(PhoneIdHelper.getPhoneId(this));

        uploaderInstance = Uploader.getInstance(this);
        lastUploadTimestamp = uploaderInstance.getLastUploadTimestamp(this);
        dataToSendTV = findViewById(R.id.data_to_send_textview);
        lastUploadDateTV = findViewById(R.id.last_upload_date_textview);
        progressBar = findViewById(R.id.upload_progress_bar);
        uploadErrorTextView = findViewById(R.id.upload_error_text_view);
        uploadButton = findViewById(R.id.upload_button);
        uploadButton.setOnClickListener(this);
        updateDataViews();
    }

    private void updateDataViews() {
        String formattedDate = lastUploadTimestamp != 0 ? DateHelper.getFormattedDate(lastUploadTimestamp, DateHelper.DEFAULT_DATE_PATTERN) : getString(R.string.uploads_never_uploaded);
        lastUploadDateTV.setText(formattedDate);
        progressBar.setVisibility(View.GONE);

        if (uploaderInstance.existsDataForUpload()) {
            // there's data to be uploaded
            int numberOfPermissionsToUpload = uploaderInstance.getNumberOfPermissionDataToUpload();
            dataToSendTV.setText(getResources().getQuantityString(R.plurals.uploads_has_data, numberOfPermissionsToUpload, numberOfPermissionsToUpload));
            uploadButton.setEnabled(true);
        } else {
            dataToSendTV.setText(getString(R.string.uploads_has_no_data));
            uploadButton.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to previous activity
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (!v.equals(uploadButton))  // for now there's only the upload button, but this is for safety
            return;

        if (!ContextualDataHolder.getInstance(UploadsActivity.this.getApplicationContext()).hasNetworkConnection()) {
            Snackbar.make(findViewById(R.id.parent_layout), R.string.no_connection_warning, Snackbar.LENGTH_LONG).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        if (uploaderInstance.existsDataForUpload()) {
            uploadButton.setEnabled(false);  // to prevent further presses while uploading
            uploadErrorTextView.setVisibility(View.GONE);

            // because the user is using the phone, the periodic uploader is not running, due to the required idle constraint.
            // Thus, this workInfoLiveData will always be the single upload work
            final LiveData<WorkInfo> workInfoLiveData = uploaderInstance.tryEnqueueSingleUploadWork(UploadsActivity.this);
            if (workInfoLiveData == null) {
                // no data to upload (this should not happen because we already check this)
                return;
            }
            workInfoLiveData.observe(UploadsActivity.this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null) {
                        if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                            MyLogHelper.logDebug(TAG, "Upload successful");
                            // update views in main thread
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    lastUploadTimestamp = uploaderInstance.getLastUploadTimestamp(UploadsActivity.this);
                                    updateDataViews();
                                }
                            });
                        } else if (workInfo.getState() == WorkInfo.State.FAILED) {
                            final Data outputData = workInfo.getOutputData();
                            if (outputData.equals(Data.EMPTY)) {
                                MyLogHelper.logError(TAG, "Upload worker failed without output of the error message. Forgot to send the output data in failure?");
                                // this only happens due to code changes in the uploader. However, we let it proceed as it is not critical
                            }
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    uploadErrorTextView.setText(
                                            getString(R.string.uploads_error_uploading, outputData.getString(Uploader.UploadWorker.ERROR_OUTPUT_DATA_KEY))
                                    );
                                    uploadErrorTextView.setVisibility(View.VISIBLE);
                                    uploadButton.setEnabled(true);
                                }
                            });
                        }
                    }
                }
            });
        }
    }
}
