package pt.uc.dei.copmode.npm.backend.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import pt.uc.dei.copmode.npm.backend.entities.CalendarEvent;

public class ListCalendarEventsConverter {
    @TypeConverter
    public static List<CalendarEvent> toList(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<CalendarEvent>>() {}.getType();
        return gson.fromJson(json, type);
    }

    @TypeConverter
    public static String fromList(List<CalendarEvent> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
