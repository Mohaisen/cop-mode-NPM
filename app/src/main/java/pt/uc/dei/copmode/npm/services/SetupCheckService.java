package pt.uc.dei.copmode.npm.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pt.uc.dei.copmode.npm.frontend.activities.SetupNPMActivity;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

@SuppressLint("SpecifyJobSchedulerIdRange")
public class SetupCheckService extends JobService {
    private static final String TAG = "PeriodicSetupCheckService";
    public static final String EXTRA_MISSING_PERMISSIONS_KEY = "extra_missing_permissions_key";
    public static final String EXTRA_REQUEST_WIFI_ALWAYS_SCAN_KEY = "extra_request_wifi_always_scan_key";


    @Override
    public boolean onStartJob(JobParameters params) {
        MyLogHelper.logDebug(TAG, TAG + " running");
        // todo: I should probably check whether the user is not using the phone before starting the activity

        ArrayList<String> missingPermissions = getMissingPermissions();

        if (!PermissionManagerService.hasUsageAccessPermission(this)) {
            missingPermissions.add(Manifest.permission.PACKAGE_USAGE_STATS);
            // this is a system app and always denied to third party apps. See PermissionManagerService.hasUsageAccessPermission()
        }

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        boolean shouldRequestAlwaysScan = !wifiManager.isScanAlwaysAvailable();

        if (missingPermissions.isEmpty() && !shouldRequestAlwaysScan) {
            jobFinished(params, true);  // always reschedule because the user can change the permissions at any time.
            return false;  // nothing left to do.
        }

        // We start the activity to request the permissions.
        Intent intent = new Intent(this.getApplicationContext(), SetupNPMActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putStringArrayListExtra(EXTRA_MISSING_PERMISSIONS_KEY, missingPermissions);
        // instead of adding the shouldRequestAlwaysScan to the intent, we always verify if it is needed in SetupNPMActivity
        this.startActivity(intent);

        // we always reschedule because the user can change the permissions at any time.
        jobFinished(params, true);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;  // reschedules job if it failed
    }

    private ArrayList<String> getMissingPermissions() {
        // returns ArrayList to send in the intent
        ArrayList<String> missingPermissions = new ArrayList<>();
        List<String> requiredPermissions = Arrays.asList(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CALENDAR);
        for (String permission: requiredPermissions) {
            int permissionStatus = checkCallingOrSelfPermission(permission);
            if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        return missingPermissions;
    }
}
