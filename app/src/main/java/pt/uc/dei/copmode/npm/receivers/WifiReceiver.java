package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;

public class WifiReceiver extends BroadcastReceiver implements ContextReceiver {
    private static final String TAG = "WifiReceiver";
    private boolean isReceiving = false;

    private static final long SCAN_PERIOD_MILLIS = 300000;  // scan every 5 minutes. We also get results from other apps' scans

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null || !action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
            return;

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> results = wifiManager.getScanResults();
        // MyLogHelper.logDebug(TAG, "Got scan results: " + results.toString());

        ContextualDataHolder.getInstance(context).updateWifiDevices(results);
    }

    @Override
    public boolean registerSelf(final Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        context.registerReceiver(this, intentFilter);
        // By registering the receiver, we will get wi-fi scans from other apps and the system

        // we can also try to start a scan. However, this is deprecated as of API level 28 and will be removed in the future.
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            // up to Q, we know this works well
            final WifiManager wifiManager = (WifiManager)
                    context.getSystemService(Context.WIFI_SERVICE);

            // schedule a periodic scan
            Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                public void run() {
                    boolean success = wifiManager.startScan();
                    // if (!success)
                        // MyLogHelper.logDebug(TAG, "Failed to start wi-fi scan");
                    // fail to start is due to either 1) wi-fi or location is off; 2) lack of location permissions; 3) throttling
                    // lack of location permissions is dealt with PeriodicSetupCheckService
                }
            };
            timer.schedule(timerTask, 0, SCAN_PERIOD_MILLIS);
        }
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }
}
