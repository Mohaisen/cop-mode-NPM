// ICheckCallingPermissionTesterService.aidl
package pt.uc.dei.copmode.npm;

// Declare any non-default types here with import statements

interface ICheckCallingPermissionTesterService {
   void test();
}
